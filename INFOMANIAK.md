# Installation sur Infomaniak

Pour une installation sur un hébergement cloud managé d'Infomaniak.

## Prérequis

N'ayant pas d'ordinateur windows je ne peux pas vous confirmer que les étapes suivante fonctionneront toutes sur windows. Il faudra peut-être adapter une ou deux étape ou utiliser un sous-système linux

## Fast installer

Via fast installer de l'hébergement cloud managé installer les applications suivante

1. BuildEssential
2. NVM

## Sur le serveur (en ssh)

1. nvm install stable
2. nvm install 12.14.0
3. npm install -g pm2

## Support infomaniak

Etant donnée que vous ne pouvez pas installer n'importe quel package sur un serveur cloud managé vous devrez demander au support d'infomaniak d'installer les packages suivants:
(N'ayez pas peur le support infomaniak est très serviable :smile:)

1. Chromium
2. libgbm-dev

## Premier deploiement avec rsync

Renommer le fichier `.npmrc-example ` en `.npmrc` et adapter les variable d'environnement à votre hébergement cloud infomaniak

Une fois les variable modifier executer les commandes suivante à la racine de ce repository (en local):

### Installer rsync

```sh
# MacOS
brew install rsync
# Linux (Debian, Ubuntu, ...)
apt-get install rsync
# Linux rpm based (Fedora, CentOS, ...)
yum install rsync
# Windows
# Sur windows veuillez utiliser linux subsystem
```

### Installer sshpass

```sh
# MacOS
brew install hudochenkov/sshpass/sshpass
# Linux (Debian, Ubuntu, ...)
sudo apt-get install sshpass
# Linux rpm based (Fedora, CentOS, ...)
yum install sshpass
# Windows
# Sur windows veuillez utiliser linux subsystem
```

### Deployer

A la racine de ce repository executer les commande suivante

```sh
# Installation des dépendances
npm install
# Deploiement
npm run send:prod
```

## Dans le dossier back

1. npm install
2. pm2 start npm --name "back-prod" -- start

## Dans le dossier front

1. npm install
2. pm2 start npm --name "back-prod" -- start

## .htaccess

```apacheconf

RewriteEngine On

# Rediriger les requêtes http sur https
RewriteCond %{HTTPS} off
RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

# Par défault apache rewrite sur index.html, cette ligne désactive cette règle
DirectoryIndex

# ===============================
#			  PROD
#    api:4040     front: 8000
# ===============================

RewriteCond %{HTTP_HOST} ^api\.majentus\.ch [NC]
RewriteRule ^(.*)$ http://localhost:4040/$1 [L,P]


RewriteCond %{HTTP_HOST} ^www\.majentus\.ch [NC]
RewriteRule ^(.*)$ http://localhost:8000/$1 [L,P]

# ===============================
#			  Staging
#    api:4051     front: 4050
# ===============================

RewriteCond %{HTTP_HOST} ^staging-api\.majentus\.ch [NC]
RewriteRule ^(.*)$ http://localhost:3000/$1 [L,P]

RewriteCond %{HTTP_HOST} ^staging\.majentus\.ch [NC]
RewriteRule ^(.*)$ http://localhost:3000/$1 [L,P]

# ===============================
#	      Autre sous-domaine
#        Rewrite to prod front
# ===============================

RewriteCond %{HTTP_HOST} ^majentus\.ch [NC]
RewriteRule ^(.*)$ http://localhost:4040/$1 [L,P]

# SECTION BEGIN GIT PROTECTION
RedirectMatch 404 /\.git
# SECTION END GIT PROTECTION

```

## Set .env file

Aller dans `back/env` et `front/env` renommer les fichier `.env-example` en `.env` et définir les variable d'environnement

Les ports doivent correspondre aux ports indiqués dans le fichier .htaccess

## Definir les process pm2

Dans le dossier du back prod

```sh
pm2 start npm --name "back-prod" -- start
```

Dans le dossier du front prod

```sh
pm2 start npm --name "back-front" -- start
```

## Futur deploiement avec rsync

Une fois toutes étapes réalisé et que le serveur de prod fonctionne comme il faut il suffira de deployer avec le commande suivante (en local)

```sh
npm run deploy:prod
```
