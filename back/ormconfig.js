require("dotenv").config();
module.exports = {
    type: "mysql",
    host: process.env.TYPEORM_DB_HOST,
    port: process.env.TYPEORM_DB_PORT,
    username: process.env.TYPEORM_DB_USER,
    password: process.env.TYPEORM_DB_PASS,
    database: process.env.TYPEORM_DB_NAME,
    synchronize: process.env.TYPEORM_DB_SYNC,
    logging: process.env.TYPEORM_DB_LOG,
    entities: ["./src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],
    cli: {
        entitiesDir: "src/entity",
        migrationsDir: "src/migration",
        subscribersDir: "src/subscriber",
    },
};
