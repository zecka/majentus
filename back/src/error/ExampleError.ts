import { HttpError } from "routing-controllers";

export class ExampleError extends HttpError {
    public operationName: string;
    public args: any[];

    constructor(operationName: string, args: any[] = []) {
        super(500);
        Object.setPrototypeOf(this, ExampleError.prototype);
        this.operationName = operationName;
        this.args = args; // can be used for internal logging
    }

    toJSON() {
        return {
            status: this.httpCode,
            failedOperation: this.operationName
        };
    }
}