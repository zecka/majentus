import { HttpError } from "routing-controllers";
/**
 * Exception for 409 HTTP error.
 * @see https://github.com/typestack/routing-controllers#throw-http-errors View all error
 */
export class ConflictError extends HttpError {
    constructor(message?: string) {
        super(409, message);
        this.name = "ConflictError";
    }
}
