import { HttpError } from "routing-controllers";
import { ValidationError } from "class-validator";
/**
 * Exception for 409 HTTP error.
 * @see https://github.com/typestack/routing-controllers#throw-http-errors View all error
 */

export class ValidationsError extends HttpError {
    private errors: Array<ValidationError>

    constructor(errors: any[] = []) {
        super(500);
        Object.setPrototypeOf(this, ValidationsError.prototype);
        this.errors = errors; // can be used for internal logging
    }
    toJSON() {
        const errors = []
        if (this.errors.length) {
            for (const validationError of this.errors) {
                errors.push({
                    property: validationError.property,
                    constraints: validationError.constraints
                })
            }
        }
        return {
            status: this.httpCode,
            name: "ValidationError",
            errors,
            failedOperation: "test"
        };
    }
}
