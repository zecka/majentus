import { Entity, PrimaryGeneratedColumn, Column, Unique, AfterLoad, getRepository } from "typeorm";
import { Exclude, Expose } from "class-transformer";
import { Ticket } from './Ticket';

@Entity()
@Unique(["email"])
export class Demo {
  constructor() {
    // Before typeorm initialization
    // Here all property is not set
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({ nullable: true, select: false })
  secret: string;

  virtual: string;

  ticket: Array<Ticket>

  @Exclude()
  virtualHidden: string;

  @AfterLoad()
  getVirtual() {
    this.virtual = "This is virtual, no in database";
    this.virtualHidden = "This is a virtual hidden, no in database, no in output json";
  }
  @AfterLoad()
  async getFromOtherRepository() {
    const ticketRepo = getRepository(Ticket)
    this.ticket = await ticketRepo.find()
  }

  @Expose()
  get virtualAlt() {
    return "alternativ for virutal property";
  }
}
