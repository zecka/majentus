import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { User } from './User';
import { GatewayInterface } from '@shared/interfaces';
import { MajentusEntity } from './MajentusEntity';

@Entity()
export class StripeAccount extends MajentusEntity implements GatewayInterface {


    get gateway() {
        return "stripe";
    }

    @PrimaryGeneratedColumn()
    id: number;

    // if we delete user we delete also stripe account
    @OneToOne((type) => User, (user) => user.stripeAccount, { onDelete: 'CASCADE' })
    @JoinColumn()
    user: User;

    @Column({ select: false, nullable: false })
    secretKey: string;

    @Column({ nullable: false })
    publicKey: string;
}