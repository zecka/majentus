import { Entity, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { MajentusEntity } from './MajentusEntity';
import { Column } from 'typeorm';
import { OneToMany } from 'typeorm';
import { Ticket } from './Ticket';
import { OrderResponse } from '@shared/interfaces/RequestDataInterface';
import { IsEmail } from 'class-validator';
@Entity()
export class Order extends MajentusEntity implements OrderResponse {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true, select: false })
    token: string;

    @Column()
    @IsEmail()
    email: string;

    @OneToMany((type) => Ticket, (ticket) => ticket.order)
    tickets: Ticket[];

    @CreateDateColumn()
    createdAt: Date;

}