import {
    Entity,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    Unique,
    OneToOne,
    getRepository,
    JoinColumn,
    JoinTable,
} from "typeorm";
import { Event } from "./Event";
import { UserItemInterface, Roles } from "@shared/interfaces";
import { Expose } from "class-transformer";
import { Length, IsNotEmpty, IsEmail, Contains, IsAlphanumeric } from "class-validator";
import * as bcrypt from "bcryptjs";
import { Media } from './Media';
import { MajentusEntity } from './MajentusEntity';
import { IsUsername } from "../validators/IsUsername";
import { StripeAccount } from "./StripeAccount";
import { ChangeEmail } from './ChangeEmail';
import { ScanAuthorization } from './ScanAuthorization';
@Entity()
@Unique(["email"])
@Unique(["username"])
export class User extends MajentusEntity implements UserItemInterface {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(4, 24)
    @IsUsername({ message: "Nom d'utilisateur invalide" })
    username: string;

    @Column({ select: false })
    @Length(4, 100)
    password: string;

    @Column({ default: "seller" })
    @IsNotEmpty()
    role: Roles = "seller";

    @Column({ name: "email" })
    @IsEmail()
    email: string;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ nullable: false })
    firstName: string;

    @Column({ nullable: false })
    lastName: string;


    @OneToMany((type) => Event, (event) => event.user)
    events: Event[];

    @OneToMany((type) => ScanAuthorization, (scanAuthorization) => scanAuthorization.user)
    scanAuthorizations: ScanAuthorization[];

    @Column({ nullable: false, default: false })
    active: boolean;

    // If we delete a Media we set avatar to null
    @OneToOne(type => Media, { onDelete: 'SET NULL', eager: true })
    @JoinColumn()
    avatar: Media;

    // if we delete stripe account we set user stipe account to null
    @OneToOne((type) => StripeAccount, (stripeAccount) => stripeAccount.user, { onDelete: 'SET NULL' })
    stripeAccount: StripeAccount;

    // if we delete ChangeEmail we set user changeEmail to null
    @OneToOne((type) => ChangeEmail, (changeEmail) => changeEmail.user, { onDelete: 'SET NULL' })
    changeEmail: ChangeEmail;



    @Expose()
    get fullName() {
        return this.firstName + " " + this.lastName;
    }

    getRepo() {
        return getRepository(User);
    }

    setPassword(password: string) {
        this.password = password;
        this.hashPassword()
    }
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }
    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
    async checkPassword(unencryptedPassword: string) {
        const userRepository = this.getRepo()
        const userWithPass = await userRepository.findOne(this.id, { select: ["password"] });
        const password = userWithPass.password;
        return bcrypt.compareSync(unencryptedPassword, password);
    }
    removeFiles() {
        const mediaRepository = getRepository(Media);
        if (this.avatar) {
            mediaRepository.remove(this.avatar)
        }
    }


}
