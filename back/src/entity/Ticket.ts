import { Entity, PrimaryGeneratedColumn, Column, Index, Double, ManyToOne, getRepository, BeforeInsert, AfterInsert, OneToMany, AfterLoad, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { EventDate } from "./EventDate";
import { User } from "./User";
import TicketToken from '../helpers/TicketToken';
import { Event } from "./Event";
import { UnauthorizedError } from 'routing-controllers';
import { Pricing } from "./Pricing";
import { Expose } from "class-transformer";
import { Order } from "./Order";
@Entity()
export class Ticket {

    constructor() {

    }
    static async create(eventDateID, pricingID, userID,) {
        const ticket = new Ticket()
        if (userID) {
            const userRepository = getRepository(User);
            const user = await userRepository.findOneOrFail(userID);
            ticket.user = user;
        }
        if (pricingID) {
            const pricingRepository = getRepository(Pricing);
            const pricing = await pricingRepository.findOne(pricingID);
            ticket.pricing = pricing;
        }
        if (eventDateID) {
            const eventDateRepository = getRepository(EventDate)
            const eventDate = await eventDateRepository.findOneOrFail(eventDateID);
            ticket.eventDate = eventDate;
        }

        return ticket;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: false })
    used: boolean

    @Column({ default: null })
    firstName: string;
    @Column({ default: null })
    lastName: string;
    @Column({ default: null })
    email: string;

    @Column({ default: null })
    token: string;

    @ManyToOne(type => Pricing)
    pricing: Pricing;

    @ManyToOne((type) => EventDate, (eventDate) => eventDate.tickets, { onDelete: 'CASCADE' })
    eventDate: EventDate;

    @ManyToOne((type) => User)
    user: User;

    @ManyToOne((type) => Order, (order) => order.tickets)
    order: Order;

    @Expose()
    event: Event;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    public async getEvent() {
        const eventRepository = getRepository(Event);
        return await eventRepository.findOne({ where: { eventDate: this.eventDate }, relations: ["user"] })
    }
}
