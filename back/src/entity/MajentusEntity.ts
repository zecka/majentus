
import { Exclude } from 'class-transformer';
import { User } from './User';
import { UnauthorizedError } from 'routing-controllers';
import { BaseEntity } from 'typeorm';
import { BadRequestError } from 'routing-controllers';
/**
 * Abstract entity
 */
export class MajentusEntity extends BaseEntity {

    user?: User;

    /**
     * Properties who don't need any traitement before update
     */
    @Exclude()
    updatables: string[]
    /**
     * Because we can not .save entity with array of id like [1,2]
     * we need to set an array of object with id key [{id:1}, {id:2}]
     * @param property 
     */
    prepareRelationSet(property) {
        if (this[property]) {
            const set = [];
            for (const relationId of this[property]) {
                set.push({ id: relationId });
            }
            this[property] = set;
        }
    }
    prepareRelation(property) {
        if (this[property]) {
            this[property] = { id: this[property] };
        }
    }
    generate(data: any): void {
        delete data.id;
        for (const property in data) {
            this[property] = data[property];
        }
    }
    setData(data: any): void {
        for (const property in data) {
            if (!this.hasOwnProperty(property)) {
                throw new BadRequestError(property + " property doesn't exist for this entity")
            }
            this[property] = data[property];
        }
    }

    updateProperties(datas) {
        const updatable = this.updatables;
        if (!updatable.length) {
            return;
        }
        for (const field of updatable) {
            if (datas[field]) {
                this[field] = datas[field]
            }
        }
    }
    async onlyOwner(currentUser: User) {

        if (currentUser.role === 'admin') {
            return true;
        }
        if (!this.user) {
            console.error("This entity doesnt load user before call onlyOwner function")
        }
        if (!this.user || currentUser.id !== this.user.id) {
            throw new UnauthorizedError("NOT_ALLOWED")
        }
    }


}