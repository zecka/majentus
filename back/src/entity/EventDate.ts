import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, AfterUpdate, AfterInsert, getRepository, AfterLoad, BeforeInsert, getCustomRepository, OneToOne } from "typeorm";
import { Event } from "./Event";
import { Ticket } from "./Ticket";
import { MajentusEntity } from "./MajentusEntity";
import { ForbiddenError } from 'routing-controllers';
import { EventStatus } from "@shared/interfaces";
import { IsEnum } from "class-validator";
import { ScanAuthorization } from "./ScanAuthorization";
import { EventDateInterface } from '../../../shared/interfaces/EventDateInterface';

@Entity()
export class EventDate extends MajentusEntity implements EventDateInterface {

    updatables: ["date"];

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    date: Date;

    sold: number
    available: number

    @Column("enum", {
        enum: EventStatus,
        default: String(EventStatus.Pending)
    })

    @IsEnum(EventStatus)
    status: EventStatus;

    // if we delete event we delete also event date
    @ManyToOne((type) => Event, (event) => event.dates, { onDelete: 'CASCADE' })
    event: Event;

    @OneToMany((type) => Ticket, (ticket) => ticket.eventDate)
    tickets: EventDate[];

    // if we delete ScanAuthorization we set eventDate.scanAuthorization to null
    @OneToOne((type) => ScanAuthorization, (scanAuthorization) => scanAuthorization.eventDate, { onDelete: 'SET NULL' })
    scanAuthorization: ScanAuthorization;

    @BeforeInsert()
    checkIfHaveEvent() {
        if (!this.event) {
            throw new ForbiddenError("an eventDate must be related to an event")
        }
    }

}
