import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { User } from './User';
import { MediaItemInterface } from '@shared/interfaces';
import { MajentusEntity } from './MajentusEntity';

@Entity()
export class ChangeEmail extends MajentusEntity {

    @PrimaryGeneratedColumn()
    id: number;

    // Delete cascade, if we delete user we delete also ChangeEmail
    @OneToOne((type) => User, (user) => user.changeEmail, { onDelete: 'CASCADE' })
    @JoinColumn()
    user: User;

    @Column({ select: false })
    token: string;

    @Column()
    cancelToken: string;

    @Column()
    email: string;

}