import { Entity, PrimaryGeneratedColumn, Column, Index, PrimaryColumn } from "typeorm";


/**
 * This entity is used to store additionnal data for other entity
 */
@Entity()
export class Metadata {


    @PrimaryColumn()
    entityName: string;

    @PrimaryColumn()
    entityId: number;

    @PrimaryColumn()
    key: string;

    @Column({ nullable: false })
    value: string;
}