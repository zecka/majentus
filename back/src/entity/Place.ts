import { Entity, PrimaryGeneratedColumn, Column, Index, Double } from "typeorm";

@Entity()
export class Place {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({ type: "double" })
    lat: number;

    @Column({ type: "double" })
    lng: number;
}
