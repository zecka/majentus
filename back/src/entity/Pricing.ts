import { Entity, PrimaryGeneratedColumn, Column, Index, Double, ManyToOne } from "typeorm";
import { Event } from "./Event";

@Entity()
export class Pricing {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    price: number;

    // if we delete event we delete also pricing
    @ManyToOne((type) => Event, (event) => event.pricings, { onDelete: 'CASCADE' })
    event: Event;


}
