import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { User } from './User';
import { MediaItemInterface } from '@shared/interfaces';

@Entity()
export class Media implements MediaItemInterface {
    constructor(file: any = null) {
        if (file) {
            this.constructorFromFile(file);
        }
    }
    constructorFromFile(file: any) {
        this.path = file.path.replace('../front/static', '');
        this.mimetype = file.mimetype
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    path: string;

    @Column()
    mimetype: string;

    @ManyToOne((type) => User, { onDelete: "CASCADE" })
    user: User;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    // GETTERS

    get file() {
        return {
            path: this.path,
            mimetype: this.mimetype
        }
    }
}
