import { Entity, PrimaryGeneratedColumn, Column, Tree, TreeChildren, TreeParent, ManyToMany, JoinTable, getConnection, BeforeInsert, AfterInsert, getRepository } from "typeorm";
import { Expose } from "class-transformer";
import { MajentusEntity } from "./MajentusEntity";
import { Event } from './Event';
import { customAlphabet } from 'nanoid'
const slug = require('slug');

@Entity()
@Tree("materialized-path")
export class Category extends MajentusEntity {
    constructor();
    constructor(title: string);
    constructor(title: string, parent: Category);
    constructor(title?: string, parent?: Category) {
        super();
        if (title) {
            this.title = title;
        }
        if (parent) {
            this.parent = parent;
        }
    }
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({ nullable: true })
    slug: string;

    // tree entity: https://github.com/typeorm/typeorm/blob/master/docs/tree-entities.md
    @Expose()
    @TreeChildren()
    children: Category[];

    @Expose()
    @TreeParent()
    parent: Category;


    @ManyToMany((type) => Event, (event) => event.categories)
    events: Event[];


    @BeforeInsert()
    private generateSlug(): void {
        const nanoid = customAlphabet('1234567890abcdef', 5)
        this.slug = slug(this.title + '-' + nanoid());
    }
}
