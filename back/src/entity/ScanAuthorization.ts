import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToOne, JoinTable, ManyToOne } from 'typeorm';
import { MajentusEntity } from './MajentusEntity';
import { EventDate } from './EventDate';
import { JoinColumn } from 'typeorm';
import { User } from './User';
@Entity()
export class ScanAuthorization extends MajentusEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    token: string;

    // Delete cascade, if we delete eventDate we delete also ScanAuthorization
    @OneToOne((type) => EventDate, (eventDate) => eventDate.scanAuthorization, { onDelete: 'CASCADE' })
    @JoinColumn()
    eventDate: EventDate;

    // Delete cascade, if we delete user we delete also event
    @ManyToOne((type) => User, (user) => user.scanAuthorizations, { onDelete: 'CASCADE' })
    @JoinTable()
    user: User;

    @CreateDateColumn()
    createdAt: Date;

}