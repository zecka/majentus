import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, OneToOne, JoinColumn } from "typeorm";
import { User } from "./User";
import { Category } from "./Category";
import { EventDate } from "./EventDate";
import { EventStatus } from "@shared/interfaces";
import { MajentusEntity } from "./MajentusEntity";
import { Media } from "./Media";
import { Length, IsEnum } from 'class-validator';
import { Pricing } from "./Pricing";
const crypto = require('crypto');
@Entity()
export class Event extends MajentusEntity {

    updatables = ["title", "categories"];
    gateway: {
        gateway: string
        publicKey: string
    }
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 120 })
    @Length(1, 120)
    title: string;

    @Column({ type: "text", nullable: true })
    description?: string;

    start: Date;

    end: Date;

    @Column({ nullable: true, select: false })
    secret: string;

    @Column()
    capacity: number

    @Column()
    duration: number

    // Delete cascade, if we delete user we delete also event
    @ManyToOne((type) => User, (user) => user.events, { onDelete: 'CASCADE' })
    user: User;

    @OneToMany((type) => EventDate, (event_date) => event_date.event)
    dates: EventDate[];

    @OneToMany((type) => Pricing, (pricing) => pricing.event, { cascade: true, eager: true })
    pricings: Pricing[];

    @ManyToMany((type) => Category, (category) => category.events, { eager: false, onUpdate: 'CASCADE' })
    @JoinTable()
    categories: Category[];


    @Column("enum", {
        enum: EventStatus,
        default: String(EventStatus.Pending)
    })
    @IsEnum(EventStatus)
    status: EventStatus;

    // If we delete a Media we set avatar to null
    @OneToOne(type => Media, { onDelete: 'SET NULL', eager: true })
    @JoinColumn()
    cover: Media;

    async genereateSecret() {
        const secret = "sk_" + crypto.randomBytes(16).toString('hex');
        // TODO: Faire attention que ce secret n'existe pas
        this.secret = secret;
    }


}
