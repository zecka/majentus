
import { Pricing } from '../entity/Pricing';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Pricing)
export class PricingRepository extends Repository<Pricing> {

}