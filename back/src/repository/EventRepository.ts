import { EntityRepository, Repository, getConnectionManager } from "typeorm";
import { Event } from "../entity/Event";
import { CategoryRepository } from "./CategoryRepository";
import { GatewayRepository } from './GatewayRepository';

@EntityRepository(Event)
export class EventRepository extends Repository<Event> {

    async upcomings() {
        const events = await Event.createQueryBuilder('event')
            .distinct(true) // dont get duplicates (event in two categories)
            .innerJoin('event.dates', 'eventdate', 'eventdate.date >= :date', { date: new Date }) // get events where category is in categories array
            .innerJoinAndSelect('event.categories', 'cat') // add categories to selected events
            .innerJoinAndSelect('event.dates', 'dates') // add categories to selected events
            .getMany()
        return events
    }
    async pasts() {
        const events = await Event.createQueryBuilder('event')
            .distinct(true) // dont get duplicates (event in two categories)
            .innerJoin('event.dates', 'eventdate', 'eventdate.date < :date', { date: new Date }) // get events where category is in categories array
            .innerJoinAndSelect('event.categories', 'cat') // add categories to selected events
            .innerJoinAndSelect('event.dates', 'dates') // add categories to selected events
            .getMany()
        return events
    }
    async updateFromJson(eventData: any) {
        const categoryRepository = getConnectionManager().get().getCustomRepository(CategoryRepository);
        const event = await this.findOne(eventData.id)
        event.generate(eventData);
        if (eventData.categories) {
            event.categories = await categoryRepository.findByIds(eventData.categories);
        }
        return await this.save(event)
    }

    // TODO: Type response
    async getOne(id: number) {
        const gatewayRepository = getConnectionManager().get().getCustomRepository(GatewayRepository);
        const event = await this.findOneOrFail(id, { relations: ["dates", "categories", "user", "cover"] });
        const gateway = await gatewayRepository.getUserGateways(event.user)
        let gatewaydata = null
        if (gateway) {
            gatewaydata = {
                gateway: gateway.gateway,
                publicKey: gateway.publicKey
            }
        } else {
            gatewaydata = {
                gateway: 'stripe',
                publicKey: process.env.STRIPE_PK
            }
        }
        return {
            ...event,
            gateway: gatewaydata
        }
    }


}