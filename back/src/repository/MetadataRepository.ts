import { EntityRepository } from 'typeorm';
import { Metadata } from '../entity/Metadata';
import { Repository } from 'typeorm';
import { User } from '../entity/User';
type MetaDataSetData = { entityName: string, entityId: number, key: string, value: string }
@EntityRepository(Metadata)
export class MetadataRepository extends Repository<Metadata> {

    async get(entityName: string, entityId: number, key: string) {
        return await this.findOneOrFail({ where: { entityName, entityId, key } })
    }

    /**
     * Insert or update metadata
     * @param entityName entity name in PascalCase
     * @param entityId 
     * @param key the key of data
     * @param value 
     */
    async set(data: MetaDataSetData) {
        return await this.save({
            entityName: data.entityName,
            entityId: data.entityId,
            key: data.key,
            value: data.value
        })
    }



}