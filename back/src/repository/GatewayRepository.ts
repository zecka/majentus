import { StripeAccount } from '../entity/StripeAccount';
import { EntityRepository, Repository, getConnectionManager } from 'typeorm';
import { Order } from '../entity/Order';
import { TicketRepository } from './TicketRepository';
import { UserRepository } from './UserRepository';
import { User } from '../entity/User';
import { GatewayInterface } from '@shared/interfaces';

/**
 * This is a multiple entity repositry
 * A the moment only one entity: StripeAccount
 * TODO: Add multiple PaymentGateway options like Unipay, Paypal, Saferpay,...
 */
@EntityRepository(StripeAccount)
export class GatewayRepository extends Repository<StripeAccount> {

    private userRepository: UserRepository;
    constructor() {
        super();
        // this.userRepository = getConnectionManager().get().getCustomRepository(UserRepository);
    }
    async getUserGateways(currentUser: User): Promise<GatewayInterface> {
        const defaultStripAccount = {
            gateway: 'stripe',
            secretKey: process.env.STRIPE_SK,
            publicKey: process.env.STRIPE_PK,
            id: 0
        }

        try {
            const stripeAccount = await this.findOneOrFail({ where: { user: currentUser }, select: ["id", "publicKey", "secretKey"] })
            if (stripeAccount) {
                return stripeAccount;
            } else {
                return defaultStripAccount;
            }
        } catch (e) {
            return defaultStripAccount
        }
    }
}