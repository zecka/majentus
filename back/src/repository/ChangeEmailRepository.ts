import { EntityRepository, Repository } from 'typeorm';
import { ChangeEmail } from '../entity/ChangeEmail';
import { User } from '../entity/User';
@EntityRepository(ChangeEmail)
export class ChangeEmailRepository extends Repository<ChangeEmail> {
    getByUser(user: User) {
        return this.findOne(({ where: { user: user } }));
    }
    getOneWithToken(id: number) {
        return this.findOne(id, { relations: ['user'], select: ['id', 'email', 'token'] })
    }
    getOneWithCancelToken(id: number) {
        return this.findOne(id, { relations: ['user'], select: ['id', 'email', 'cancelToken'] })
    }
}