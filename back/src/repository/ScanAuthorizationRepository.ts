import { ScanAuthorization } from '../entity/ScanAuthorization';
import { EntityRepository, Repository, getConnectionManager, DeepPartial, getConnection } from 'typeorm';
import { User } from '../entity/User';
import { EventDateRepository } from './EventDateRepository';
import { ScanAuthorizationInterface } from '@shared/interfaces';
@EntityRepository(ScanAuthorization)
export class ScanAuthorizationRepository extends Repository<ScanAuthorization> {
    getByUser(currentUser: User) {
        return this.find({ where: { user: currentUser }, relations: ["eventDate", "eventDate.event"] })
    }
    getByDateID(eventDateId: number): Promise<ScanAuthorization> {
        return this.findOne({ where: { eventDate: { id: eventDateId } }, relations: ["eventDate", "eventDate.event"] })
    }
    getOne(id: number) {
        return this.findOneOrFail(id, { relations: ["eventDate", "eventDate.event"] })
    }
    getByToken(token: string): Promise<ScanAuthorization> {
        return this.findOne({ where: { token: token }, relations: ["eventDate", "eventDate.event"] });
    }
}