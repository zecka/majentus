import { EntityRepository, Repository, getCustomRepository, In, getConnectionManager } from "typeorm";
import { Event } from "../entity/Event";
import { User } from "../entity/User";
import { Category } from "../entity/Category";
import { Ticket } from '../entity/Ticket';
import { EventRepository } from './EventRepository';
import { EventDateRepository } from './EventDateRepository';
import { EventDate } from '../entity/EventDate';

@EntityRepository(Ticket)
export class TicketRepository extends Repository<Ticket> {

    private eventRepo: EventRepository;
    private eventDateRepo: EventDateRepository;
    constructor() {
        super();
        this.eventRepo = getConnectionManager().get().getCustomRepository(EventRepository);
        this.eventDateRepo = getConnectionManager().get().getCustomRepository(EventDateRepository);
    }

    async getByEventId(eventId: number) {
        const event = await this.eventRepo.findOne(eventId, { relations: ["dates"] });
        const tickets = await this.find({
            where: { eventDate: In(event.dates.map(date => date.id)) },
            relations: ["eventDate", "eventDate.event"]
        })
        return tickets;
    }
    async getByEventDateid(eventDateId: number) {
        const eventDate = await this.eventDateRepo.findOne(eventDateId, { relations: ["tickets"] })
        return eventDate.tickets
    }

    async getBySellerId(sellerId: number, relations: string[] = ["eventDate", "eventDate.event"]) {
        const dates = await this.eventDateRepo.getBySellerId(sellerId);
        if (!dates.length) {
            return []
        }
        const tickets = await this.find({
            where: { eventDate: In(dates.map(date => date.id)) },
            relations: relations
        })
        return tickets;
    }


}