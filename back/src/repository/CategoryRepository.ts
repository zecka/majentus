import { EntityRepository, Repository, getCustomRepository } from "typeorm";
import { Event } from "../entity/Event";
import { User } from "../entity/User";
import { Category } from "../entity/Category";

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {

}