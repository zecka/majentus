import { EntityRepository, Repository } from "typeorm";
import { Order } from '../entity/Order';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {

    public async getOne(orderId: number): Promise<Order> {
        return await this.findOneOrFail({ where: { id: orderId }, relations: ["tickets", "tickets.pricing"], select: ["token", "id", "email"] });
    }

}