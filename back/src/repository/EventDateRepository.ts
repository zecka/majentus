import { EntityRepository, Repository, MoreThan, LessThan, getCustomRepository, In, getConnectionManager } from "typeorm";
import { EventDate } from "../entity/EventDate";
import { EventRepository } from './EventRepository';
import { UpdateEventDateRequest } from "@shared/interfaces/RequestDataInterface";
import { User } from "../entity/User";
import { NotFoundError, UnauthorizedError, BadRequestError } from "routing-controllers";
import { NewEventDateRequest } from '../../../shared/interfaces/RequestDataInterface';
import { EventStatus } from "@shared/interfaces";
import { Event } from "../entity/Event";
@EntityRepository(EventDate)
export class EventDateRepository extends Repository<EventDate> {
    private eventRepository: EventRepository;
    constructor() {
        super();
        this.eventRepository = getConnectionManager().get().getCustomRepository(EventRepository);
    }

    upcomings() {
        return this.find({
            relations: ["event", "event.categories"],
            where: {
                date: MoreThan(new Date())
            },
            order: {
                date: "ASC"
            }
        });
    }

    pasts() {
        return this.find({
            relations: ["event", "event.categories"],
            where: {
                date: LessThan(new Date())
            },
            order: {
                date: "DESC"
            }
        });
    }
    one(id: number) {
        return this.findOne(id, { relations: ["event", "event.categories"] });
    }

    /**
     * Get all event date of a given user id
     * @param sellerId user id
     * @param relations relations to be returned with eventDate entities
     */
    async getBySellerId(sellerId: number, relations: string[] = []) {
        // Get all event of this seller
        const events = await this.eventRepository.find({ where: { user: sellerId } });
        if (!events.length) return [];
        // Get ids conditions with all event id like : {where:{event: In(1,4,6)}}
        const options: any = { where: { event: In(events.map(e => e.id)) } };
        if (relations.length) {
            options.relations = relations;
        }
        const dates = await this.find(options)
        return dates;
    }

    async relatedEvent(id: number): Promise<Event> {
        const eventDate = await this.findOne(id, { relations: ["event"] });
        return eventDate.event
    }
    async getByEventId(id: number): Promise<EventDate[]> {
        return this.find({
            where: {
                event: id
            }
        })
    }

}