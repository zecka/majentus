import { EntityRepository, Repository, getConnectionManager } from "typeorm";
import { User } from "../entity/User";
import { EventRepository } from "./EventRepository";
import { UserAvailabilityRequest, UserAvailabilityResponse, UpdateUserRequest } from '@shared/interfaces/RequestDataInterface';
import { InjectRepository } from "typeorm-typedi-extensions";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    @InjectRepository(EventRepository)
    private eventRepository: EventRepository;


    findAll() {
        return this.find(
            {
                relations: ["avatar", "changeEmail"],
                order: { id: "DESC" },
            },
        );
    }

    async getBasicData(id: number): Promise<UpdateUserRequest> {
        const user = await this.findOne(id, { relations: ["changeEmail", "avatar"] });
        return {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            changeEmail: user.changeEmail,
            avatar: user.avatar
        }
    }




    findEventsByUserId(id: number) {
        return this.eventRepository.find({
            relations: ["categories"],
            where: {
                user: {
                    id: id,
                },
            },
        });
    }

    /**
     * Check availability of email and/or username
     * @param info 
     */
    async availability(info: UserAvailabilityRequest): Promise<UserAvailabilityResponse> {
        let emailAvailable = null;
        let usernameAvailable = null;
        if (info.email) {
            const email = await this.findOne({ where: { email: info.email } })
            emailAvailable = (email) ? false : true;
        }
        if (info.username) {
            const username = await this.findOne({ where: { username: info.username } })
            usernameAvailable = (username) ? false : true;
        }
        return {
            email: emailAvailable,
            username: usernameAvailable
        }
    }
}