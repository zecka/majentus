import { MajentusEntity } from "../entity/MajentusEntity";
import { validate, ValidationError } from 'class-validator';
import { ForbiddenError } from "routing-controllers";
import { validateOrReject } from 'class-validator';

export class AbstractValidator {
    private errors: [] = [];

    protected static async validateOrReject(entity: MajentusEntity) {
        return validateOrReject(entity)
    }
    protected static async validate(entity: MajentusEntity) {
        return validate(entity)
    }
}