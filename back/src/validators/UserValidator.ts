import { User } from "../entity/User";
import { validateOrReject } from 'class-validator';
import MediaHelpers from '../helpers/MediaHelpers';
import { UnauthorizedError } from 'routing-controllers';
import { AbstractValidator } from "./AbstractValidator";
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '../repository/UserRepository';

export class UserValidator extends AbstractValidator {
    static async validateOrReject(user: User) {
        await validateOrReject(user);
        if (user.avatar) {
            if (! await MediaHelpers.userCanUseMedia(user, user.avatar)) {
                throw new UnauthorizedError("This user can not use this media")
            }
        }
    }

    static async update(user: User) {
    }
    static async register(user: User) {
        await this.validateOrReject(user);
        
    }
}