import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function IsUsername(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isUsername',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
            // Accept alphanumeric, dot, underscore, tiret
            const regex = /^[A-Za-z0-9\._-]+$/
            return regex.test(value);
        },
      },
    });
  };
}