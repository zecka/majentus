import { Event } from "../entity/Event";
import { AbstractValidator } from "./AbstractValidator";
import { UnauthorizedError } from 'routing-controllers';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { User } from "../entity/User";
import { getCustomRepository } from 'typeorm';
import { EventRepository } from '../repository/EventRepository';
import { ValidationsError } from "../error/ValidationErrors";

export class EventValidator extends AbstractValidator {
    static common(updateData: any) {
        if (updateData.start || updateData.end) {
            throw new UnauthorizedError("You need to set eventdate items to define event start/end date")
        }
    }
    static async update(updateData: any, currentUser: User) {
        const eventRepo = getCustomRepository(EventRepository);
        const event: Event = await eventRepo.findOne(updateData.id, { "relations": ['cover', 'categories', 'user'] })
        this.common(updateData)
        event.setData(updateData);
        const errors = await this.validate(event);
        if (errors.length) {
            throw new ValidationsError(errors)
        }
        await event.onlyOwner(currentUser);

        this.checkIfCategoryExist(event)
        return event;
    }
    static async insert(event: any) {
        const eventEntity = plainToClass(Event, event);
        const errors = await this.validate(eventEntity);
        if (errors.length) {
            throw new ValidationsError(errors)
        }
        this.checkIfCategoryExist(eventEntity)
    }

    static checkIfCategoryExist(event: Event) {
        // TODO: Implememt checkIfCategoryExist event validation
    }
}