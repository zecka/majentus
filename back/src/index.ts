
import "reflect-metadata";
import { createConnection, useContainer as useContainerForOrm } from "typeorm";
import { useContainer as useContainerForRouting } from 'routing-controllers';

import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";

import { useExpressServer } from "routing-controllers";
import Controllers from "./controllers/";
import helmet = require("helmet");
import { checkJwt } from "./middlewares/CheckJwt";
import { ErrorHandler } from "./middlewares/ErrorHandler";
import { currentUserChecker } from './middlewares/currentUserChecker';
import { initHelpers } from "./helpers/InitHelpers";
import { MajentusSeeder } from "./seeders/Seeder";
import { Container } from "typedi";


useContainerForOrm(Container);
useContainerForRouting(Container);
createConnection()
    .then(async (connection) => {
        // create express app
        const app = express();
        app.use(cors());
        app.use(helmet());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        // for parsing multipart/form - data

        useExpressServer(app, {
            currentUserChecker,
            authorizationChecker: checkJwt,
            defaultErrorHandler: false,
            middlewares: [ErrorHandler],
            // register created express server in routing-controllers
            controllers: Controllers, // and configure it the way you need (controllers, validation, etc.)
        });

        // setup express app here
        // ...

        // start express server
        app.listen(process.env.TYPEORM_EXPRESS_PORT);
        // RUN SEEDERS
        /*
         await connection.synchronize(true);
         const seeder = new MajentusSeeder(connection);
         await seeder.run();
         */
        initHelpers();

        console.log(`Express server has started on port ${process.env.TYPEORM_EXPRESS_PORT}. Open http://localhost:${process.env.TYPEORM_EXPRESS_PORT}/users to see results`);
    })
    .catch((error) => console.log(error));
