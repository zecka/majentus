import { Action } from 'routing-controllers';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import * as jwt from "jsonwebtoken";
import { extractTokenFromHeader } from './CheckJwt';

export const currentUserChecker = async (action: Action): Promise<User | undefined> => {
    // here you can use request/response objects from action
    // you need to provide a user object that will be injected in controller actions
    // demo code:
    const userRepository = getRepository(User)
    let userId = undefined;
    try {
        const token = extractTokenFromHeader(action.request)
        let jwtPayload;
        jwtPayload = jwt.verify(token, process.env.TYPEORM_JWT_SECRET);
        userId = jwtPayload.data.userId;
        const user = await userRepository.findOneOrFail(userId);
        return user;
    } catch (error) {
        return undefined;
    }

}