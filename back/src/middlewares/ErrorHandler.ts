import { Middleware, ExpressErrorMiddlewareInterface, InternalServerError } from "routing-controllers";

@Middleware({ type: "after" })
export class ErrorHandler implements ExpressErrorMiddlewareInterface {
    public error(error: any, request: any, response: any, next: (err: any) => any) {
        console.log("ErrorHandler", error)
        if (response.headersSent) {
            return;
        }
        if (error.httpCode) {
            if (error.errors) {
                this.validationErrorResponse(error, response);
            } else {
                this.defaultErrorResponse(error, response);
            }
        } else if (error && error.name === "QueryFailedError") {
            this.sqlErrorResponse(error, response)
        }
    }
    private defaultErrorResponse(error, response) {
        response.status(error.httpCode).json({
            ...error
        });
    }
    private validationErrorResponse(error, response) {
        response.status(error.httpCode).json({ errors: error.errors });
    }
    private sqlErrorResponse(error, response) {
        response.status(500).json({
            message: "Erreur SQL"
        });
    }
}
