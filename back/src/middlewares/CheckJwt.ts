// https://medium.com/javascript-in-plain-english/creating-a-rest-api-with-jwt-authentication-and-role-based-authorization-using-typescript-fbfa3cab22a4
// https://www.ninjadevcorner.com/2019/12/no-passportjs-nodejs-rest-api-authentication-with-typescript.html
import { Action, UnauthorizedError } from "routing-controllers";
import * as jwt from "jsonwebtoken";
import { Request } from "express";
export const extractTokenFromHeader = (req: Request) => {
    if (req.headers.authorization && req.headers.authorization.split(" ")[0] === "Bearer") {
        return req.headers.authorization.split(" ")[1];
    }
};


export const checkJwt = async (action: Action, roles: string[]) => {
    const { request, response } = action;

    //extract the jwt token from the Authorization header
    const token = extractTokenFromHeader(request);
    let jwtPayload;

    //Try to validate the token and get data
    try {
        jwtPayload = jwt.verify(token, process.env.TYPEORM_JWT_SECRET);
        response.locals.jwtPayload = jwtPayload;
    } catch (error) {
        //response token is not valid, respond with 401 (unauthorized)
        throw new UnauthorizedError("You should be logged in to access this url");
    }

    if ((roles.length && !roles.includes(jwtPayload.data.role)) && !roles.includes("all")) {
        // Check if user roles is allowed;
        throw new UnauthorizedError("You are not allow")
    }
    //We refresh the token on every request by setting another 2h
    try {
        const {
            data: { userId, username, role },
        } = jwtPayload;
        const newToken = jwt.sign({ data: { userId, username, role } }, process.env.TYPEORM_JWT_SECRET, {
            expiresIn: "2h",
        });
        response.setHeader("Authorization", "Bearer " + newToken);
    } catch (error) {
        throw new UnauthorizedError("Wrong token format");
    }

    return true;
};
