import { Controller, Get, Param, Post, Body, Delete, Authorized, CurrentUser, Put } from "routing-controllers";
import { User } from "../entity/User";
import { UpdateEventDateRequest } from "@/../shared/interfaces";
import { NewEventDateRequest } from '../../../shared/interfaces/RequestDataInterface';
import { EventDateService } from '../services/EventDateService';
import { Inject } from "typedi";

@Controller("/event-date")
export class EventDateController {

    @Inject() service: EventDateService

    @Get()
    async all() {
        return this.service.findAll()
    }

    @Authorized(['all'])
    @Put()
    async update(@Body() eventDateData: UpdateEventDateRequest, @CurrentUser() currentUser: User) {
        return await this.service.update(eventDateData, currentUser)
    }

    @Authorized(['all'])
    @Post()
    async create(@Body() eventDate: NewEventDateRequest, @CurrentUser() currentUser: User) {
        return this.service.create(eventDate, currentUser);
    }

    @Get("/upcomings")
    async getUpcomingDates() {
        return this.service.upcomings();
    }

    @Get("/pasts")
    async getPastDates() {
        return this.service.pasts();
    }

    @Get("/date/:id")
    async one(@Param("id") id: number) {
        return this.service.one(id);
    }


    @Get("/date/:id/event")
    async getEvent(@Param("id") id: number) {
        return this.service.relatedEvent(id);
    }

    @Delete("/date/:id")
    @Authorized(['all'])
    async remove(@Param("id") id: number, @CurrentUser() currentUser: User) {
        return this.service.delete(id, currentUser);
    }

}
