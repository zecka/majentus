import { Response, Request } from "express";
import { Controller, Body, Post, Res, Req, NotFoundError, BadRequestError, UnauthorizedError, CurrentUser, Get } from "routing-controllers";
import { getRepository, getCustomRepository, getConnectionManager } from "typeorm";
import { User } from "../entity/User";
import * as jwt from "jsonwebtoken";
import { UserRepository } from '../repository/UserRepository';
import { Inject } from 'typedi';
import { AuthService } from '../services/AuthService';

@Controller("/auth")
export class AuthController {

    @Inject()
    service: AuthService

    @Post("/login")
    async login(@Body() body: any, @Res() res: Response) {
        //Check if username and password are set
        const { username, password } = body;
        return this.service.login(username, password);

    }
    @Post('/logout')
    async logout() {
        return { message: "Nothing to do in backend at the moment" }
    }

    @Get("/user")
    async getLoggedUser(@CurrentUser() user: User) {
        if (!user) {
            return {
                user: false,
                logged: false,
            }
        }
        return { user, logged: true };
    }

}
