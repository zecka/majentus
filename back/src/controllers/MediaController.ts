import { Controller, Get, Post, UploadedFile, CurrentUser, Delete, Param } from "routing-controllers";
import { User } from '../entity/User';
import FileUploader from '../helpers/FileUpload';
import { Media } from '../entity/Media';
import { getRepository, getConnectionManager } from "typeorm";
import { Inject } from "typedi";
import { MediaService } from '../services/MediaService';

// @see https://stackoverflow.com/questions/33279153/rest-api-file-ie-images-processing-best-practices
const fileUploader = new FileUploader();
@Controller("/media")
export class MediaController {

    @Inject() service: MediaService

    @Get()
    async all() {
        return this.service.findAll()
    }

    @Post("/image")
    async saveFile(@UploadedFile("image", fileUploader.options) image: any, @CurrentUser() user: User) {
        return this.service.create(image, user)
    }
    @Delete("/:id")
    async remove(@Param("id") id: number, @CurrentUser() user: User) {
        return this.service.delete(id, user);
    }
}
