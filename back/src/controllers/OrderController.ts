import { Controller, Get, Res, Param } from 'routing-controllers';
import { OrderResponse } from "@shared/interfaces/RequestDataInterface";
import { Inject } from 'typedi';
import { OrderService } from '../services/OrderService';

@Controller("/orders")
export class OrderController {

    @Inject()
    service: OrderService;

    @Get("/:orderId/:orderToken")
    async sendOrderData(@Param('orderId') orderId: number, @Param('orderToken') orderToken: string): Promise<OrderResponse> {
        const order = await this.service.getOne(orderId, orderToken);
        return order;
    }

    @Get("/zip/:orderId/:orderToken")
    async sendZip(@Param('orderId') orderId: number, @Param('orderToken') orderToken: string, @Res() response) {
        const zip = this.service.zipOrder(orderId, orderToken)
        this.zipResponse(response, zip);
    }

    @Get("/pdf/:orderId/:ticketId/:orderToken")
    async sendPdf(
        @Param('orderId') orderId: number,
        @Param('ticketId') ticketId: number,
        @Param('orderToken') orderToken: string,
        @Res() response) {
        const pdf = this.service.ticketPdf(orderId, ticketId, orderToken)
        return this.pdfResponse(response, pdf);
    }

    pdfResponse(response, pdf, filename: string = "ticket") {
        response.contentType("application/pdf")
        return pdf;
    }
    zipResponse(response, zip, zipFileName: string = "tickets") {
        zip.pipe(response);
        response.writeHead(200, {
            'Content-Type': 'application/zip',
            'Content-disposition': `attachment; filename=${zipFileName}.zip`
        });
        zip.finalize();
    }
}