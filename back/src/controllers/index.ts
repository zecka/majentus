import { DemoController } from "./DemoController";
import { UserController } from "./UserController";
import { EventController } from "./EventController";
import { CategoryController } from "./CategoryController";
import { AuthController } from "./AuthController";
import { MediaController } from "./MediaController";
import { TicketController } from "./TicketController";
import { EventDateController } from "./EventDateController";
import { PrinterController } from './PrinterController';
import { PricingController } from './PricingController';
import { OrderController } from './OrderController';
import { ChangeEmailController } from "./ChangeEmailController";
import { GatewayController } from './GatewayController';
import { StatsController } from './StatsController';
import { ScanAuthorizationController } from './ScanAuthorizationController';
import { ContactController } from './ContactController';
export default [
    DemoController,
    UserController,
    EventController,
    CategoryController,
    AuthController,
    MediaController,
    TicketController,
    EventDateController,
    PrinterController,
    PricingController,
    OrderController,
    ChangeEmailController,
    GatewayController,
    StatsController,
    ScanAuthorizationController,
    ContactController
];
