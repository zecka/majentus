import {
    Controller,
    Body,
    Get,
    Authorized,
    Post,
    CurrentUser,
    BadRequestError,
} from "routing-controllers";

import { User } from "../entity/User";
import { BuyTicketRequestData, CreateTicketRequestData, ScanResult, ScanTicketRequest, OrderResponse } from "@shared/interfaces";
import { OrderService } from '../services/OrderService';
import { Inject } from "typedi";
import { TicketService } from '../services/TicketService';


@Controller("/tickets")
export class TicketController {

    @Inject()
    private orderService: OrderService

    @Inject()
    private service: TicketService


    @Get("/seller")
    @Authorized()
    async getTicketOfCurrentSeller(@CurrentUser() currentUser: User) {
        return this.service.getSellerTickets(currentUser)
    }

    @Post("/buy")
    async buyTickets(@Body() data: BuyTicketRequestData): Promise<OrderResponse> {
        return await this.orderService.create(data, data.token);
    }
    @Post("/create")
    @Authorized()
    async postTickets(@Body() data: CreateTicketRequestData, chargeToken: string = ""): Promise<OrderResponse> {
        return this.orderService.create(data, chargeToken);
    }


    @Post("/scan")
    async scan(@Body() data: ScanTicketRequest, @CurrentUser() currentUser: User): Promise<ScanResult> {
        return this.service.scan(data, currentUser)
    }

}
