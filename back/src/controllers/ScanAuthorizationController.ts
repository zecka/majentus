import { Controller, Get, Param, CurrentUser, Post, Delete, Authorized, Body } from "routing-controllers";
import { User } from '../entity/User';
import { ScanAuthorizationService } from "../services/ScanAuthorizationService";
import { ScanAuthorizationInterface, NewAuthorizationRequest } from "@shared/interfaces";
import { ScanAuthorization } from "../entity/ScanAuthorization";
import { Inject } from "typedi";

@Controller("/scan-authorization")
export class ScanAuthorizationController {

    @Inject()
    service: ScanAuthorizationService;

    @Get('/current-user')
    getCurrentUserAuthorizations(@CurrentUser() currentUser: User): Promise<ScanAuthorization[]> {
        return this.service.getUserAuthorizations(currentUser);
    }
    @Get('/one/:dateId')
    getEventDateAuthorization(@Param('dateId') eventDateId: number, @CurrentUser() currentUser: User): Promise<ScanAuthorizationInterface> {
        return this.service.getDateAuthorization(eventDateId, currentUser);
    }
    @Post('/add')
    @Authorized(['all'])
    createEventDateAuthorization(@Body() data: NewAuthorizationRequest, @CurrentUser() currentUser: User) {
        return this.service.createDateAuthorization(data.eventDate, currentUser);
    }
    @Delete('/:authorizationId')
    deleteEventDateAuthorization(@Param('authorizationId') authorizationId: number, @CurrentUser() currentUser: User) {
        return this.service.deleteAuthorization(authorizationId, currentUser);
    }
    @Get('/getbytoken/:token')
    getByToken(@Param('token') token: string, @CurrentUser() currentUser: User) {
        return this.service.getByToken(token);
    }

}