
import { Controller, Get, CurrentUser, Authorized } from 'routing-controllers';
import { StatService } from '../services/StatsServices';
import { User } from '../entity/User';
import { Inject } from 'typedi';

@Controller("/statistiques")
export class StatsController {

    @Inject()
    service: StatService;

    @Get('/monthly-sales')
    @Authorized(['all'])
    monthlySales() {
        return this.service.getSellerMonthlySells(1, 1)
    }

    @Get('/dates-pricing')
    @Authorized(['all'])
    datesPricingStats(@CurrentUser() currentUser: User) {
        try {
            return this.service.getEventDatesPricingStats(currentUser.id);
        } catch (e) {
            console.log("ERROR", e)
        }
    }
}