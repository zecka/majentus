import { Controller, Get, CurrentUser, Authorized, Put, Body, Post } from 'routing-controllers';
import { User } from '../entity/User';
import { GatewayService } from '../services/GatewayService';
import { StripeEntityRequest, GatewayInterface } from '@shared/interfaces';
import { Inject } from 'typedi';

@Controller("/gateways")
export class GatewayController {

    @Inject()
    service: GatewayService;

    @Get('/current')
    @Authorized('all')
    async getUserGateway(@CurrentUser() currentUser: User): Promise<GatewayInterface> {
        return this.service.getUserGateways(currentUser);
    }

    @Put('/stripe')
    async updateStripeGetway(@Body() data: StripeEntityRequest, @CurrentUser() currentUser: User) {
        return this.service.updateStripe(data, currentUser);
    }
    @Post('/stripe')
    async createStripeAccount(@Body() data: StripeEntityRequest, @CurrentUser() currentUser: User) {
        return this.service.createStripeAccount(data, currentUser);
    }
}