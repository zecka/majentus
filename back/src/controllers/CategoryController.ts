import { Controller, Param, Get, Post, Put, Delete, Params } from "routing-controllers";
import { getManager, TreeRepository, In, getConnectionManager } from "typeorm";
import { Category } from "../entity/Category";
import { CategoryRepository } from "../repository/CategoryRepository";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Inject } from "typedi";
import { CategoryService } from "../services/CategoryService";

@Controller("/categories")
export class CategoryController {

    @Inject()
    private service: CategoryService;

    @Get()
    async getAll() {
        return this.service.findAll();
    }
    @Get("/roots")
    async getRoots() {
        return this.service.findRoots()
    }
    @Get("/one/:id")
    async getCategory(@Param("id") id: number) {
        return this.service.getById(id);
    }

}

