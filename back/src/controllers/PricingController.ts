import { Controller, Post, Body, CurrentUser, Put, Delete, Param } from "routing-controllers";

import { User } from "../entity/User";
import { Inject } from "typedi";
import { PricingService } from '../services/PricingService';

@Controller("/pricings")
export class PricingController {

    @Inject()
    private service: PricingService


    @Post()
    async post(@Body() data: any, @CurrentUser() currentUser: User) {
        return this.service.create(data, currentUser);
    }

    @Put()
    async put(@Body() data: any, @CurrentUser() currentUser: User) {
        return this.service.update(data, currentUser);
    }

    @Delete("/:id")
    async trash(@Param("id") id: number, @CurrentUser() currentUser: User) {
        return this.service.delete(id, currentUser);
    }
}