import { Controller, Post, Body } from "routing-controllers";
import { Inject } from "typedi";
import { EmailService } from '../services/EmailService';
import { ContactRequest } from '@shared/interfaces';

@Controller("/contact")
export class ContactController {

    @Inject() emailService: EmailService

    @Post('/')
    contact(@Body() contactRequestData: ContactRequest) {
        return this.emailService.contact(contactRequestData)
    }
}