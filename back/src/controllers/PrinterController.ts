import { Controller } from 'routing-controllers';
import { Get } from 'routing-controllers';
import { Param } from 'routing-controllers';
import { Res } from 'routing-controllers';
import { TicketPrinter } from '../helpers/TicketPrinter';
@Controller("/printer")
export class PrinterController {
    // TODO: Make this controller more secure
    @Get("/ticket/:id/ticket.pdf")
    async print(@Param("id") id: number, @Res() res) {
        res.contentType("application/pdf");
        const printer = await TicketPrinter.create(id);
        return printer.pdf()
    }
    @Get("/ticket/:id/display")
    async preview(@Param("id") id: number, @Res() res) {
        res.contentType("text/html");
        const printer = await TicketPrinter.create(id);
        return printer.display()
    }

}