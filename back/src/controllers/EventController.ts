import { Controller, Get, Param, Post, Body, Delete, Authorized, CurrentUser, Put } from "routing-controllers";
import { User } from "../entity/User";
import { Inject } from "typedi";
import { EventService } from "../services/EventService";

@Controller("/events")
export class EventController {

    @Inject() service: EventService;

    @Get()
    async all() {
        return this.service.findAll()
    }
    @Get("/:id([0-9]+)")
    one(@Param("id") id: number) {
        return this.service.getOne(id)
    }

    @Post()
    @Authorized(['all'])
    async create(@Body() eventData: any, @CurrentUser() user: User) {
        return this.service.create(eventData, user)
    }
    @Put()
    @Authorized(['all'])
    async update(@Body() eventData: any, @CurrentUser() currentUser: User) {
        return this.service.update(eventData, currentUser);
    }

    @Get("/current-user")
    @Authorized(['all'])
    async eventCurrentUser(@CurrentUser() currentUser: User) {
        return this.service.currentUserEvents(currentUser);
    }
    @Get("/upcomings")
    async getUpcomingsEvents() {
        return this.service.upcomings()
    }
    @Get("/pasts")
    async getPastsEvents() {
        return this.service.pasts()
    }


    /**
     * 
     * @param id eventId
     */
    @Get("/:id([0-9]+)/dates")
    async getDates(@Param("id") id: number) {
        this.service.getDates(id);
    }
    @Get("/:id([0-9]+)/tickets")
    async getTicketByEventId(@Param("id") eventId: number) {
        return this.service.getTickets(eventId);
    }

    @Delete("/:id")
    @Authorized(['all'])
    async remove(@Param("id") id: number, @CurrentUser() currentUser: User) {
        return this.service.delete(id, currentUser);
    }


}
