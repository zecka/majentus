import { Controller, Get, Param } from "routing-controllers";
import { ChangeEmailService } from '../services/ChangeEmailService';
import { Inject } from "typedi";

@Controller("/change-email")
export class ChangeEmailController {

    @Inject()
    private changeEmailservice: ChangeEmailService


    @Get('/validate/:id/:token')
    async validate(@Param('id') changeEmailId: number, @Param('token') token: string) {
        await this.changeEmailservice.validateChangeEmail(changeEmailId, token)
        return true;
    }

    @Get('/cancel/:id/:token')
    async cancel(@Param('id') changeEmailId: number, @Param('token') token: string) {
        await this.changeEmailservice.cancelChangeToken(changeEmailId, token)
        return true;
    }

}
