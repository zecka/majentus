import { Controller, Get } from "routing-controllers";

import { Email } from '../helpers/email/Mail';
import { Inject } from 'typedi';
import { OrderService } from '../services/OrderService';
import { OrderMail } from '../helpers/email/OrderMail';
import { MajentusMail } from "../helpers/email/MajentusMail";
import { TicketService } from '../services/TicketService';
import TicketPrinter from "../helpers/TicketPrinter";

@Controller("/demo")
export class DemoController {

    @Inject() orderService: OrderService
    @Inject() ticketService: TicketService


    @Get('/email')
    async emailDemo() {

        const preview = "This is a preview text"
        let content = "This is demo email ";
        content += await Email.button({ label: "Click here", href: "http://google.com" });

        const email = new Email({
            content,
            preview,
            to: "",
            from: { name: "", email: "" },
            subject: ""
        });

        return email.getContent()
    }
    @Get('/email-order')
    async emailOrder() {
        const order = await this.orderService.getOneWhithoutToken(1)
        const email = new OrderMail(order)
        await email.setRequirements()
        return email.getContent()
    }
    @Get('/email-validate-change')
    async emailValidateChange() {
        let content = "<p>Cliquez sur le lien ci-dessous pour valider votre nouvelle adresse email:</p>";
        content += await MajentusMail.button({ label: "Confirmer mon adresse email", href: "#" })
        const mailer = new MajentusMail({
            to: "johndoe@doe.com",
            content: content,
            subject: "Validez votre nouvel adresse email"
        })
        return mailer.getContent()
    }
    @Get('/ticket')
    async ticketDisplay() {
        const ticket = await this.ticketService.getOne(1);
        const printer = new TicketPrinter(ticket)
        return printer.display()
    }

}
