import { User } from "../entity/User";
import { Controller, Get, Param, Post, Body, Delete, Put, CurrentUser } from "routing-controllers";
import { EntityFromBody } from "typeorm-routing-controllers-extensions";
import { UserAvailabilityRequest, UpdateUserRequest } from "@shared/interfaces/RequestDataInterface";
import { Inject } from "typedi";
import { UserService } from '../services/UserService';
@Controller("/users")
export class UserController {


    @Inject()
    service: UserService

    @Get()
    async all() {
        // Only for debug purpose
        // TODO: DELETE THIS return
        return this.service.findAll();
    }
    @Get("/data/:id")
    one(@Param("id") id: number) {
        return this.service.findOne(id);
    }
    @Delete("/:id")
    async remove(@Param("id") id: number, @CurrentUser() currentUser: User) {
        return this.service.trash(id, currentUser);
    }
    @Get("/:id/events")
    async getUserEvents(@Param("id") id: number) {
        return this.service.findEventsByUserId(id);
    }

    @Post("/register")
    async register(@EntityFromBody() user: User) {
        return this.service.register(user);
    }

    @Post("/availability")
    async availability(@Body() info: UserAvailabilityRequest) {
        return this.service.availability(info);
    }


    @Get("/update/basic")
    async getBasicData(@CurrentUser() currentUser: User): Promise<any> {
        return this.service.getBasicData(currentUser.id)
    }

    @Put("/update")
    async update(@Body() updateData: UpdateUserRequest, @CurrentUser() currentUser: User) {
        return this.service.update(updateData, currentUser);
    }

    @Get("/resend-activation/:username")
    async resendActivation(@Param("username") username: string): Promise<string> {
        return this.service.resendActivationCode(username);
    }


    @Get("/activation/:username/:activationCode")
    async activation(@Param("username") username: string, @Param("activationCode") activationCode: string): Promise<string> {
        return this.service.activation(activationCode, username);
    }

}
