import { getRepository } from 'typeorm';
import { Event } from '../entity/Event';
import { User } from '../entity/User';
import { Category } from '../entity/Category';
import { EventDate } from '../entity/EventDate';
import { Ticket } from '../entity/Ticket';
import { Roles, EventStatus } from '@shared/interfaces';
import { Demo } from '../entity/Demo';
import { Pricing } from '../entity/Pricing';
import { getCustomRepository } from 'typeorm';
import { Metadata } from '../entity/Metadata';
import { MetadataRepository } from '../repository/MetadataRepository';
import { asyncForEach } from '../helpers/utils';

const faker = require("faker");
const CategoriesJSON = require("./categories.json");

export class MajentusSeeder {
    private userRepo = getRepository(User)
    private categoryRepo = getRepository(Category)
    private eventRepo = getRepository(Event)
    private eventDateRepo = getRepository(EventDate)
    private ticketRepo = getRepository(Ticket)
    private demoRepo = getRepository(Demo)
    private manager;

    private users = [];
    private categories = [];
    private events = [];
    private eventDates = [];

    private passBypassSonar = "12345678"

    constructor(connection) {
        this.manager = connection.manager
    }

    async run() {
        await this.insertDemoEntity()
        console.log("SEEDER INSERT ROOT USER")
        await this.insertSellerUser()
        console.log("SEEDER INSERT SELLER USER")
        await this.insertUnactiveUser()
        console.log("SEEDER INSERT INNACTIVE USER")
        await this.insertRootUser();
        console.log("SEEDER INSERT USER")
        await this.insertUsers(10);
        console.log("SEEDER INSErT CATS")
        await this.insertCategories();
        console.log("SEEDER USER FIND")
        this.users = await this.userRepo.find();
        console.log("SEEDER START CATEGORY FIND")
        this.categories = await this.categoryRepo.find();
        console.log("SEEDER START EVENT")
        await this.insertEvents();
        console.log("SEEDER START EVENT FIND")
        this.events = await this.eventRepo.find();
        console.log("SEEDER START EVENTDATE")
        await this.insertEventDates()
        console.log("SEEDER START EVENDATE TODAY")
        await this.insertEventDateToday();
        console.log("SEEDER START TICKET")
        await this.insertTickets();
        console.log("SEEDER START METADATA")
        await this.insertMetadata();
        console.log('\x1b[36m%s\x1b[0m', "Seeder success")
        return true
    }

    async insertDemoEntity() {
        const demo = new Demo();
        demo.email = faker.internet.email()
        demo.name = faker.name.findName()
        await this.demoRepo.insert(demo);
        return true
    }
    async insertRootUser() {
        const user = new User();
        user.firstName = "Robin";
        user.lastName = "Ferrari";
        user.username = "root";
        user.email = "robin.ferrari@crea-inseec.com";
        user.role = "admin";
        user.active = true;
        user.password = this.passBypassSonar;
        await this.userRepo.insert(user);
        return true;
    }
    async insertSellerUser() {
        const user = new User();
        user.firstName = "John";
        user.lastName = "Doe";
        user.username = "seller";
        user.email = "jd@gmail.com";
        user.role = "seller";
        user.active = true;
        user.password = this.passBypassSonar;
        await this.userRepo.insert(user);
        return true;
    }
    async insertUnactiveUser() {
        const user = new User();
        user.firstName = "unactive";
        user.lastName = "Man";
        user.username = "unactive";
        user.email = "unactive@gmail.com";
        user.role = "seller";
        user.active = false;
        user.password = this.passBypassSonar;
        await this.userRepo.insert(user);
        return true;
    }
    async insertUsers(nbUsers) {
        for (let i = 0; i < nbUsers; i++) {
            const user = new User();
            const { name } = faker;
            user.firstName = name.firstName();
            user.lastName = name.lastName();
            user.username = faker.internet.userName(user.firstName, user.lastName);
            user.email = faker.internet.email(user.firstName, user.lastName);
            user.role = "seller";
            user.password = this.passBypassSonar;
            await this.userRepo.insert(user);
        }
        return true

    }
    async insertCategories() {
        for (const cat of CategoriesJSON) {
            const parent = new Category(cat.title);
            await this.manager.save(parent);
            if (cat.children) {
                await asyncForEach(cat.children, async (child) => {
                    const categoryChild = new Category(child.title, parent);
                    await this.manager.save(categoryChild);
                    if (child.children && child.children.length) {
                        await asyncForEach(child.children, async (sub) => {
                            const subchild = new Category(sub.title, categoryChild);
                            await this.manager.save(subchild);
                        })
                    }
                });
            }
        }
        return true
    }
    async insertEvents() {
        for (let i = 0; i < this.users.length; i++) {
            const event = new Event()
            event.title = faker.company.companyName();
            event.status = EventStatus.Open;
            event.description = "dummyDescription";
            if ([1, 5, 8, 10].includes(i)) {
                event.user = new User();
                event.user.id = 1;
            } else {
                event.user = this.randomInArray(this.users).id;
            }
            event.capacity = this.randomBool() ? 50 : 150;
            event.duration = 120;
            const cat1 = new Category();
            cat1.id = this.randomInArray(this.categories).id
            const cat2 = new Category();
            cat2.id = cat1.id === 3 ? 4 : 3
            event.categories = [cat1, cat2];
            const regularPricing = new Pricing();
            regularPricing.title = "Plein tarifs"
            regularPricing.price = 45;
            const halfPricing = new Pricing();
            halfPricing.title = "Tarifs réduit"
            halfPricing.price = 30;
            event.pricings = [regularPricing, halfPricing];
            await this.manager.save(event);

        }
        return true
    }
    async insertEventDates() {
        for (const event of this.events) {
            const future = this.randomBool();
            const multipleDays = this.randomBool();
            const refDate = (future) ? faker.date.future(1) : faker.date.past(2);
            const endDate = new Date(refDate);
            await this.manager.save(
                await this.manager.create("EventDate", {
                    date: refDate,
                    event: { id: event.id },
                    status: EventStatus.Open
                })
            );
            if (multipleDays) {
                endDate.setDate(endDate.getDate() + 1);
                await this.manager.save(
                    await this.manager.create("EventDate", {
                        date: endDate,
                        event: { id: event.id },
                        status: EventStatus.Open
                    })
                );
            }
        }

        return true
    }
    async insertEventDateToday() {
        const refDate = new Date()
        const startDate = new Date(refDate);
        const endDate = new Date(refDate);
        startDate.setDate(startDate.getDate() - 1);
        endDate.setDate(endDate.getDate() + 1);

        let eventID = this.randomInArray(this.events).id
        await this.manager.save(
            this.manager.create("EventDate", {
                date: startDate,
                event: eventID,
                status: EventStatus.Open
            })
        );
        await this.manager.save(
            this.manager.create("EventDate", {
                date: endDate,
                event: eventID,
                status: EventStatus.Open
            })
        );

        const fiveHourAfter = new Date();
        fiveHourAfter.setHours(fiveHourAfter.getHours() + 5);
        eventID = this.randomInArray(this.events).id
        await this.manager.save(
            this.manager.create("EventDate", {
                date: refDate,
                event: eventID,
                status: EventStatus.Open
            })
        );
    }
    async insertTickets() {
        const dates = await this.eventDateRepo.find({ relations: ['event', 'event.pricings'] });
        for (const date of dates) {
            let nbTicket = this.randomInt(date.event.capacity)
            if (date.id === 1) {
                nbTicket = 1;
            }

            for (let j = 0; j < nbTicket; j++) {
                const pricing: Pricing = this.randomInArray(date.event.pricings);
                const name = {
                    first: faker.name.firstName(),
                    last: faker.name.lastName()
                }
                await this.manager.save(
                    this.manager.create("Ticket", {
                        eventDate: date.id,
                        pricing: pricing.id,
                        email: faker.internet.email(name.first, name.last),
                        firstName: name.first,
                        lastName: name.last,
                        createdAt: faker.date.past(1, new Date(date.date))
                    })
                );
            }
        }
    }

    async insertMetadata() {
        const repo = getCustomRepository(MetadataRepository);
        const meta = new Metadata();
        meta.key = "activationCode"
        meta.entityName = "User"
        meta.entityId = 1
        meta.value = "fakeActivator"
        await repo.insert(meta);

        meta.value = "newvalue";
        await repo.save(meta);

    }

    randomInt(max: number, min: number = 1) {
        return Math.floor(Math.random() * max) + min;
    }
    randomBool() {
        return Math.random() >= 0.5;
    }
    randomInArray(array) {
        return array[this.randomInt(array.length, 0)]
    }
}