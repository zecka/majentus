
import "reflect-metadata";
import { createConnection } from "typeorm";
import { MajentusSeeder } from "./Seeder";
createConnection().then(async (connection) => {
    await connection.synchronize(true);
    const seeder = new MajentusSeeder(connection);
    await seeder.run();
    connection.close()
}).catch((error) => console.log(error));
