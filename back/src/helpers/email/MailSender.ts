// https://subscription.packtpub.com/book/application_development/9781786468710/12/ch12lvl1sec71/sending-mail
import { Transporter, createTransport, getTestMessageUrl } from "nodemailer";
import { Email } from "./Mail";
import SMTPTransport = require("nodemailer/lib/smtp-transport");

export class MailSender {
    private static instance: MailSender;
    private transporter: Transporter;

    constructor() {
        this.transporter = createTransport(this.getTransport());
    }
    private getTransport(): SMTPTransport.Options {
        if (process.env.TYPEORM_ENV === 'production') {
            return this.infomaniak;
        } else {
            return this.ethereal;
        }
    }
    get ethereal(): SMTPTransport.Options {
        return {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'casper11@ethereal.email',
                pass: 'BKyqKY5j5WK7ap3z2n'
            }
        }
    }
    get infomaniak(): SMTPTransport.Options {
        return {
            host: process.env.TYPEORM_SMTP_HOST,
            port: Number(process.env.TYPEORM_SMTP_PORT),
            auth: {
                user: process.env.TYPEORM_SMTP_USER,
                pass: process.env.TYPEORM_SMTP_PASS,
            }
        }
    }
    get mailTrap() {
        return {
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
                user: "d6cc4a0c4ac184",
                pass: "8acf2cf15ac2da"
            }
        }
    }
    public static getInstance(): MailSender {
        if (!MailSender.instance) {
            MailSender.instance = new MailSender();
        }
        return MailSender.instance;
    }


    async send(email: Email) {
        // send mail with defined transport object
        let info = await this.transporter.sendMail({
            from: email.getFrom(), // sender address
            to: email.getTo(), // list of receivers
            subject: email.subject, // Subject line
            html: await email.getContent(), // html body
            attachments: email.attachements
        });

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", getTestMessageUrl(info));
    }
}
