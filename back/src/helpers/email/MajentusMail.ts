import { Email } from './Mail';
import { IMajentusMailConstructor } from './Email';

export class MajentusMail extends Email {
    constructor(mail: IMajentusMailConstructor) {
        super({
            to: mail.to,
            subject: mail.subject,
            content: mail.content,
            from: {
                name: "Majentus",
                email: "no-reply@majentus.ch"
            },
        })
    }
}
