import { Ticket } from "../../entity/Ticket";
import { Email } from './Mail';
import { Event } from '../../entity/Event';
import { EventDate } from '../../entity/EventDate';
import { getCustomRepository } from 'typeorm';
import { asyncForEach } from "../utils";
import { TicketPrinter } from "../TicketPrinter";
import { Order } from "@/src/entity/Order";
import { TicketRepository } from '../../repository/TicketRepository';
import * as moment from 'moment';

export class OrderMail extends Email {
    private ticket: Ticket | null;
    private tickets: Ticket[];
    private event: Event;
    private eventDate: EventDate;
    private order: Order;
    constructor(order: Order) {
        super({
            from: {
                name: "Majentus",
                email: "no-reply@majentus.ch"
            },
            // subject, to and content will be define in async function (setRequirements)
            subject: "",
            to: "",
            content: ""
        })
        this.order = order;
    }
    get orderUrl() {
        return process.env.TYPEORM_FRONT_URL + '/order/' + this.order.id + '/' + this.order.token;
    }
    async setRequirements() {
        await this.setRelations();
        this.setTo();
        this.setSubject();
        await this.setContent();
    }
    async setRelations() {
        const repo = getCustomRepository(TicketRepository);
        this.ticket = await repo.findOne(this.order.tickets[0].id, { relations: ["eventDate", "eventDate.event"] });
        this.eventDate = this.ticket.eventDate;
        this.event = this.eventDate.event;
    }
    private setTo() {
        this.to = this.order.email;
    }
    private setSubject() {
        this.subject = `Votre billet pour ${this.event.title}`
    }
    private async setContent() {
        this.content = `<h1>Merci pour votre achat !</h1> <p>Vous trouverez ci-dessous un lien pour télécharger vos billets pour:</p> <p><strong>${this.event.title}</strong> <br /> ${moment(this.eventDate.date).format("MM/DD/YYYY HH:mm")}</p>`
        this.content += await this.getOrderLink();
        this.preview = 'Vous trouverez ci-dessous un lien pour télécharger vos billets';
    }

    private async getOrderLink(): Promise<string> {
        return Email.button({ label: "Télécharger vos billets", href: this.orderUrl })
    }
    private async setAttachments() {
        await asyncForEach(this.tickets, async (ticket) => {
            const printer = await TicketPrinter.create(ticket.id);
            const pdfContent = await printer.pdf();
            this.attachements.push({
                filename: printer.filename,
                content: pdfContent,
                contentType: 'application/pdf' // optional, would be detected from the filename
            })
        })
    }
    public async send() {
        await this.setRequirements();
        await super.send();
    }
}