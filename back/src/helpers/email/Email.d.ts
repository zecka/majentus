export interface IEmailFrom {
    name: string,
    email: string
}

export interface IEmailConstructor {
    to: string | string[],
    from: IEmailFrom,
    subject: string,
    content: string,
    preview?: string,
    replyTo?: string
}

export interface IMajentusMailConstructor {
    to: string | string[],
    subject: string,
    content: string,
}