import { ForbiddenError } from 'routing-controllers';
import { MailSender } from './MailSender';
import { IEmailFrom, IEmailConstructor } from './Email';
const { TwingEnvironment, TwingLoaderArray } = require('twing');
import * as fs from 'fs'
import * as path from 'path'
import * as juice from 'juice'

export class Email {
    public to: string | string[];
    public from: IEmailFrom;
    public subject: string;
    public content: string;
    public preview: string;
    public replyTo: string;

    public attachements: any[] = [];
    private sender: MailSender;

    constructor(email: IEmailConstructor) {
        this.to = email.to;
        if (email.from) {
            this.from = {
                name: email.from.name,
                email: email.from.email
            };
        }
        this.subject = email.subject;
        this.content = email.content;
        this.preview = email.preview;
        this.replyTo = email.replyTo;
        this.sender = MailSender.getInstance();
    }
    public async send() {
        if (!this.checkRequirements()) {
            throw new ForbiddenError("")
        }
        await this.sender.send(this);
    }
    private checkRequirements(): boolean {
        return true;
    }
    public getFrom(): string {
        // return `"Fred Foo 👻" <foo@example.com>`
        return `"${this.from.name}" <${this.from.email}>`;
    }
    public getTo(): string {
        if (this.to instanceof Array) {
            return this.to.join(', ')
        }
        return this.to;
    }
    public async getContent(): Promise<string> {
        let twigContent = fs.readFileSync(path.resolve(__dirname, './templates/email.template.twig'), 'utf8')
        let loader = new TwingLoaderArray({
            'template.twig': twigContent
        });
        let twing = new TwingEnvironment(loader);

        const content = await twing.render('template.twig', { content: this.content, subject: this.subject, preview: this.preview });
        const inlineContent = juice(content, { preserveMediaQueries: true, preservePseudos: true })
        return inlineContent
    }
    static async button(button: { label: string, href: string }): Promise<string> {
        let twigContent = fs.readFileSync(path.resolve(__dirname, './templates/email.button.twig'), 'utf8')
        let loader = new TwingLoaderArray({
            'button.twig': twigContent
        });
        let twing = new TwingEnvironment(loader);
        return twing.render('button.twig', button);
    }

}