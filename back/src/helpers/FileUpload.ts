import * as multer from 'multer';
// import * as path from 'path';
const fs = require('fs');
const mkdirp = require('mkdirp')
const slugify = require('slugify')

// Create folders if not exist
mkdirp.sync('../front/static/uploads/image')
mkdirp.sync('../front/static/uploads/avatar')
mkdirp.sync('../front/static/uploads/events')

export class FileUploader {
    public options = {}
    constructor() {
        this.options = {
            options: {
                storage: multer.diskStorage({
                    destination: this.destination,
                    filename: this.filename,
                })
            }
        }
    }
    destination(req: any, file: any, cb: any) {
        const dir = '../front/static/uploads/' + file.fieldname + "/"
        cb(null, dir);
    }
    filename(req: any, file: any, cb: any) {
        const uniquePrefix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniquePrefix + "-" + slugify(file.originalname).toLowerCase())
    }
    public deleteFile(file: any) {
        FileUploader.delete(file.path);
    }
    static delete(path) {
        try {
            fs.unlinkSync('../front/static' + path)
        } catch (err) {
            console.error(err)
        }
    }

}
export default FileUploader

export const fileUploadOptions = new FileUploader().options;
