import { Ticket } from '../entity/Ticket';
import { TicketRepository } from '../repository/TicketRepository';
import { getCustomRepository } from 'typeorm';
import * as puppeteer from 'puppeteer';
import { Event } from '../entity/Event';
import { Pricing } from '../entity/Pricing';
import slugify from 'slugify';
import { asyncForEach } from './utils';
import { TwingEnvironment, TwingLoaderArray } from 'twing';
import * as QRCode from "qrcode-svg";
import * as Archiver from 'archiver';
import * as fs from 'fs';
import * as path from 'path';
import * as moment from 'moment';
import "moment/locale/fr"

declare var window: any;

export class TicketPrinter {
    private ticket: Ticket;
    private pricing: Pricing;
    private event: Event;
    private width: number = 100 * 7;
    private height: number = 40 * 7;
    public filename: string;

    constructor(ticket: Ticket) {
        this.ticket = ticket;
        if (!ticket.eventDate || !ticket.eventDate.event) {
            throw "TicketPrinter must be instanciate with pricing, evenDate and eventDate.event relation"
        }
        this.event = ticket.eventDate.event;
        this.pricing = ticket.pricing;
        this.setFilename()
    }

    public static createPdf = async (ticketId: number): Promise<Buffer> => {
        const ticket = await TicketPrinter.create(ticketId);
        return ticket.pdf();
    }
    public static createZip = async (ticketIds: number[]): Promise<Archiver.Archiver> => {
        const zip = Archiver('zip');
        await asyncForEach(ticketIds, async (id) => {
            const pdf = await TicketPrinter.createPdf(id);
            zip.append(pdf, { name: 'ticket-' + id + '.pdf' })
        })
        return zip;
    }


    public static create = async (id: number) => {
        const repo: TicketRepository = getCustomRepository(TicketRepository);
        const ticket = await repo.findOne(id, { relations: ["eventDate", "eventDate.event", "pricing"] });
        return new TicketPrinter(ticket);
    }
    public async pdf() {
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
            ]
        });
        const page = await browser.newPage();
        await page.setViewport({
            width: this.width,
            height: this.height,
            deviceScaleFactor: 3
        });
        await page.goto(`${process.env.TYPEORM_BACK_URL}/printer/ticket/${this.ticket.id}/display`);

        const pdf = await page.pdf({
            // path: 'test.pdf', // add path to save file in storage
            width: this.width + 'px',
            height: this.height + 'px',
            printBackground: true
        });
        await browser.close();
        return pdf;
    }

    public async getTemplate() {
        let twigContent = fs.readFileSync(path.resolve(__dirname, 'ticketTemplate.twig'), 'utf8')
        let styleTwigContent = fs.readFileSync(path.resolve(__dirname, 'ticketStyle.twig'), 'utf8')
        let loader = new TwingLoaderArray({
            'index.twig': twigContent,
            'style.twig': styleTwigContent
        });
        let twing = new TwingEnvironment(loader);
        const render = await twing.render('index.twig', this.getTicketData());
        return render
    }
    public async display() {
        return await this.getTemplate()
    }
    public getCover() {
        if (this.event.cover) {
            return process.env.TYPEORM_FRONT_URL + this.event.cover.path
        } else {
            return "https://images.unsplash.com/photo-1501281668745-f7f57925c3b4?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1600&h=900&fit=crop&ixid=eyJhcHBfaWQiOjF9"
        }
    }
    public getTicketData() {
        return {
            qrCode: this.getQrCode(),
            title: "Billet pour " + this.event.title + " | " + this.ticket.firstName,
            eventTitle: this.event.title,
            cover: this.getCover(),
            date: moment(this.ticket.eventDate.date).format("dddd D MMMM YYYY HH:mm"),
            firstName: this.ticket.firstName,
            lastName: this.ticket.lastName,
            email: this.ticket.email,
            primaryColor: "#9733ee",
            id: String(this.ticket.id).padStart(8, '0'),
            pricing: {
                title: this.pricing.title,
                price: this.pricing.price + " CHF"
            }
        }
    }
    public getQrCode() {
        var qrcode = new QRCode({
            content: this.ticket.token + " | " + this.ticket.id,
            padding: 0,
            join: true,
            container: "svg-viewbox", //Useful but not required,
            color: "#000000",
            background: "transparent",
            ecl: "M",
        });
        return qrcode.svg()
    }
    private setFilename() {
        this.filename = slugify(this.event.title) + "-" + this.ticket.id + ".pdf"
    }

}

export default TicketPrinter;