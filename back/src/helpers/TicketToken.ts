import { Event } from '../entity/Event';
import { Repository } from 'typeorm';
import { getRepository } from 'typeorm';
const crypto = require('crypto')

import * as jwt from "jsonwebtoken";
import { Ticket } from '../entity/Ticket';
import { EventDate } from '../entity/EventDate';


export default class TicketToken {
    private ticket: Ticket;
    constructor(ticket: Ticket) {
        this.ticket = ticket;
    }

    async generate(): Promise<string> {
        const secret: string = await this.getTicketSecret();
        // TODO: Utiliser un certificat
        const token = jwt.sign({
            data: {
                id: this.ticket.id,
                date: this.ticket.eventDate.id,
            }
        }, secret);

        return token;
    }

    async verify(token: string) {
        const secret: string = await this.getTicketSecret();
        try {
            const jwtPayload = jwt.verify(token, secret);
            return jwtPayload;
        } catch (error) {
            console.error(error);
            return false
        }
    }

    private async getEventSecret(): Promise<string> {
        const eventDateRepository = getRepository(EventDate);
        const eventDate = await eventDateRepository.findOne(this.ticket.eventDate.id, { relations: ['event'] });
        const eventRepository = getRepository(Event);
        const event = await eventRepository.findOne(eventDate.event.id, { select: ['secret'] });
        return event.secret;
    }
    private async getTicketSecret(): Promise<string> {
        const eventSecret = await this.getEventSecret();
        return eventSecret + ':' + this.ticket.id;
    }

}