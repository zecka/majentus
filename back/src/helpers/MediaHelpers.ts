import { User } from "../entity/User";
import { Media } from "../entity/Media";
import { getRepository, Repository } from 'typeorm';
import { NotFoundError } from 'routing-controllers';

export default class MediaHelpers {
    private static mediaRepo: Repository<Media>;

    /**
     * Init will be call on app init just after connection ready
     * (we can't getRepository before connection)
     */
    static init() {
        if (!this.mediaRepo) {
            this.mediaRepo = getRepository(Media);
        }
    }
    static async setUserMedia(mediaID, user: User) {
        if (mediaID) {
            const itemMedia: Media = await MediaHelpers.mediaRepo.findOne(mediaID);
            itemMedia.user = user;
            MediaHelpers.mediaRepo.save(itemMedia);
        }
    }
    static async userCanUseMedia(user: User, media: number|Media){
       const mediaID : number = (media instanceof Media ) ? media.id : media
       const itemMedia : Media = await MediaHelpers.mediaRepo.findOne(mediaID, {relations: ["user"]}) 
       if(!itemMedia.user){
           return true
       }
       return user.id === itemMedia.user.id;
    }

    /**
     * Get media by id and remove another one if exist
     * @param newMediaID 
     * @param oldMedia 
     */
    static async getNewRemoveOld(newMediaID: number, oldMedia?: Media) : Promise<Media>{
        const media = await this.mediaRepo.findOne(newMediaID)
        if (!media) {
            throw new NotFoundError("Avatar media id doesnt exist")
        }
        if (oldMedia) {
            await this.mediaRepo.remove(oldMedia);
        }
        
        return media;
    }
}
