import { InjectRepository } from "typeorm-typedi-extensions";
import { Category } from "../entity/Category";
import { TreeRepository } from "typeorm";
import { Event } from '../entity/Event';

export class CategoryService {
    // View doc: https://github.com/typeorm/typeorm/blob/master/docs/tree-entities.md#working-with-tree-entities
    @InjectRepository(Category)
    private repository: TreeRepository<Category>;

    public findAll() {
        return this.repository.findTrees();
    }
    public findRoots() {
        return this.repository.findRoots();
    }
    public async getById(id: number) {
        const category = await this.repository.findOne(id);
        category.events = await this.getEventsCategory(category)
        return category;
    }

    async getEventsCategory(category: Category): Promise<Event[]> {
        const categories = await this.repository.findDescendants(category); // gets all children
        const ids = categories.map(cat => cat.id) // get an array of ids
        const events = await Event.createQueryBuilder('event')
            .distinct(true) // dont get duplicates (event in two categories)
            .innerJoin('event.categories', 'category', 'category.id IN (:...ids)', { ids }) // get events where category is in categories array
            .innerJoinAndSelect('event.categories', 'cat') // add categories to selected events
            .orderBy('event.id') // order by event.id
            .getMany()
        return events
    }

}