import { Service, Inject } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { TicketRepository } from '../repository/TicketRepository';
import { User } from '../entity/User';
import { CreateTicketRequestData, PricingRequestData, ScanTicketRequest } from "@/../shared/interfaces";
import { BadRequestError, NotFoundError, UnauthorizedError } from "routing-controllers";
import { Ticket } from "../entity/Ticket";
import { PricingRepository } from '../repository/PricingRepository';
import { ScanAuthorizationService } from './ScanAuthorizationService';
import TicketToken from "../helpers/TicketToken";
@Service()
export class TicketService {
    @InjectRepository(TicketRepository)
    private repository: TicketRepository

    @InjectRepository(TicketRepository)
    private pricingRepository: PricingRepository

    @Inject()
    private scanAuthorizationService: ScanAuthorizationService

    getSellerTickets(user: User) {
        return this.repository.getBySellerId(user.id)
    }
    async getOne(id: number) {
        return this.repository.findOne(id, { relations: ["pricing", "eventDate", "eventDate.event"] });
    }
    async createTicketsFromRequest(data: CreateTicketRequestData) {
        const tickets = []
        for (let j = 0; j < data.pricing.length; j++) {
            const pricing = data.pricing[j];
            for (let i = 0; i < pricing.quantity; i++) {
                const ticket = await this.createTicket(pricing, data);
                tickets.push(ticket);
            }
        }
        return tickets;
    }
    async createTicket(pricingRequest: PricingRequestData, data: CreateTicketRequestData) {
        const pricing = await this.pricingRepository.findOne(pricingRequest.id, { relations: ["event"] });
        if (!pricing) {
            throw new BadRequestError("Pricing not found");
        }
        if (pricing.event.id !== data.eventDate.event.id) {
            throw new BadRequestError("Pricing ID is not related to event ID");
        }
        const ticket = await Ticket.create(data.eventDate.id, pricing.id, null);
        ticket.firstName = data.firstName;
        ticket.lastName = data.lastName;
        ticket.email = data.email;
        return ticket;
    }
    async saveTickets(tickets: Ticket[]) {
        const savedTickets = [];
        await Promise.all(
            tickets.map(async (ticket) => {
                const saved = await this.repository.save(ticket);
                savedTickets.push(saved);
            })
        );
        return savedTickets;
    }

    async scan(data: ScanTicketRequest, currentUser: User) {
        const { authorization, ticketToken, ticketId } = data;
        const ticket = await this.repository.findOne(ticketId, { relations: ["eventDate", "eventDate.event", "eventDate.event.user"] });

        if (!ticket) {
            throw new NotFoundError("TICKET_NOT_FOUND")
        }

        const authorisationObject = await this.scanAuthorizationService.getByToken(authorization);
        if (ticket.eventDate.id !== authorisationObject.eventDate.id) {
            return {
                payload: null,
                message: `Ce billet n'est pas pour la bon événement`,
                ticket: ticket,
                type: "error"
            };
        }
        await this.scanAuthorizationService.checkIfIsAllowToScan(ticket, authorization, currentUser);


        const ticketTokenObject = new TicketToken(ticket);
        const payload = await ticketTokenObject.verify(ticketToken);


        if (!payload) {
            throw new UnauthorizedError("INVALID_TICKET")
        }

        if (ticket.used) {
            return {
                payload: null,
                message: "Ce billet à déjà été scanné",
                ticket: ticket,
                type: "warning"
            };
        }

        ticket.used = true;
        await this.repository.save(ticket);
        return {
            payload,
            message: "Billet valide",
            ticket: ticket,
            type: "success"
        };

    }
}