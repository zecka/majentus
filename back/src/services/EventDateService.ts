import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { EventDateRepository } from '../repository/EventDateRepository';
import { UpdateEventDateRequest, EventStatus, NewEventDateRequest } from "@shared/interfaces";
import { EventDate } from "../entity/EventDate";
import { User } from '../entity/User';
import { NotFoundError, BadRequestError, UnauthorizedError } from "routing-controllers";
import { EventRepository } from '../repository/EventRepository';
import { Event } from "../entity/Event";

@Service()
export class EventDateService {
    @InjectRepository(EventDateRepository)
    private repository: EventDateRepository;
    @InjectRepository(EventRepository)
    private eventRepository: EventRepository;

    public findAll() {
        return this.repository.find({ relations: ["event", "event.categories", "event.pricings"], order: { date: "ASC" } });
    }
    async create(data: NewEventDateRequest, currentUser: User) {

        if (!data.date || !data.event || !data.status) {
            throw new BadRequestError("MISSING_PARAMETER");
        }
        const event = await this.eventRepository.findOneOrFail(data.event, { relations: ["user"] });
        if (!event) {
            throw new NotFoundError("EVENT_NOT_FOUND");
        }
        await event.onlyOwner(currentUser);

        const eventDate = new EventDate();
        eventDate.date = new Date(data.date);
        eventDate.event = event;
        const status = data.status as EventStatus;
        eventDate.status = status;
        return this.repository.save(eventDate);
    }

    async update(eventDateData: UpdateEventDateRequest, currentUser: User) {
        if (!eventDateData.id) {
            throw new BadRequestError("MISSING_PARAMETER")
        }
        const eventDate: EventDate = await this.repository.findOne(eventDateData.id, { relations: ["event"] });
        if (!eventDate) {
            throw new NotFoundError("Event date " + eventDateData.id + " Doesn't exist")
        }
        if (eventDateData.date) {
            eventDate.date = new Date(eventDateData.date);
        }
        if (eventDateData.status) {
            eventDate.status = eventDateData.status as EventStatus;
        }
        const event = await this.eventRepository.findOneOrFail(eventDate.event.id, { relations: ["user"] });
        if (!event) {
            throw new NotFoundError("Event " + eventDate.event + " doesnt exist");
        }

        await event.onlyOwner(currentUser);

        return this.repository.save(eventDate);
    }
    async delete(id: number, currentUser: User) {
        if (!id) {
            throw new BadRequestError("MISSING_PARAMETER")
        }
        let eventDateToRemove = await this.repository.findOne(id, { relations: ["event", "tickets"] });
        if (!eventDateToRemove) {
            throw new NotFoundError("EVENT_DATE_NOT_FOUND")
        }
        const event = await this.eventRepository.findOneOrFail(eventDateToRemove.event.id, { relations: ["user"] });
        if (!event) {
            throw new NotFoundError("EVENT_NOT_FOUND");
        }
        // Check if date alreay have ticket
        if (eventDateToRemove.tickets.length) {
            throw new UnauthorizedError("EVENTDATE_HAVE_TICKETS");
        }

        await event.onlyOwner(currentUser);

        return this.repository.remove(eventDateToRemove);
    }
    relatedEvent(id: number): Promise<Event> {
        return this.repository.relatedEvent(id)
    }
    upcomings() {
        return this.repository.upcomings()
    }
    pasts() {
        return this.repository.pasts()
    }
    one(id: number) {
        return this.repository.one(id)
    }
}