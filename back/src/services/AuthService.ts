
import { Service } from 'typedi';
import { BadRequestError, NotFoundError, UnauthorizedError } from 'routing-controllers';
import { User } from '../entity/User';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { UserRepository } from '../repository/UserRepository';
import * as jwt from "jsonwebtoken";


@Service()
export class AuthService {

    @InjectRepository(UserRepository)
    private userRepository: UserRepository;

    async login(username: string, password: string) {
        //Check if username and password are set
        if (!(username && password)) {
            throw new BadRequestError("You need to provide password and username");
        }

        //Get user from database
        let user: User;
        try {
            user = await this.userRepository.findOneOrFail({ where: [{ username }, { email: username }], select: ["id", "password", "username", "role", "active"] });
        } catch (error) {
            throw new NotFoundError("USER_NOT_FOUND");
        }
        if (!user.active) {
            throw new UnauthorizedError("UNACTIVE_USER");
        }
        //Check if encrypted password match
        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
            throw new UnauthorizedError("INVALID_PASSWORD");
        }

        const token = this.generateJwt(user);

        return {
            user: await this.userRepository.findOneOrFail({ where: { username }, relations: ['avatar'] }),
            token,
        };
        //Send the jwt in the response
    }
    generateJwt(user: User) {
        return jwt.sign({ data: { userId: user.id, username: user.username, role: user.role } }, process.env.TYPEORM_JWT_SECRET, {
            // expiresIn: "6h", // remove this directive if no expires time
        });
    }
}