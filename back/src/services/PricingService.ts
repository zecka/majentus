import { Service } from 'typedi';
import { NewPricingRequest, UpdatePricingRequest } from '@shared/interfaces';
import { User } from '../entity/User';
import { BadRequestError, NotFoundError, UnauthorizedError } from 'routing-controllers';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { EventRepository } from '../repository/EventRepository';
import { Pricing } from '../entity/Pricing';
import { PricingRepository } from '../repository/PricingRepository';
import { TicketRepository } from '../repository/TicketRepository';
@Service()
export class PricingService {
    @InjectRepository(EventRepository)
    private eventRepository: EventRepository;

    @InjectRepository(TicketRepository)
    private ticketRepository: TicketRepository;

    @InjectRepository(PricingRepository)
    private repository: PricingRepository;

    async create(data: NewPricingRequest, currentUser: User) {
        if (!data.eventId || !data.title || !data.price) {
            throw new BadRequestError("MISSING_PARAMETER");
        }
        const event = await this.eventRepository.findOne(data.eventId, { relations: ["user"] })
        if (!event) {
            throw new NotFoundError("EVENT_NOT_FOUND");
        }
        await event.onlyOwner(currentUser);

        const pricing = new Pricing();
        pricing.price = data.price;
        pricing.title = data.title;
        pricing.event = event;
        await this.repository.insert(pricing);

        return true;
    }

    async update(data: UpdatePricingRequest, currentUser: User) {
        if (!data.id) {
            throw new BadRequestError("MISSING_PARAMETER");
        }
        const pricing = await this.repository.findOne(data.id, { relations: ["event", "event.user"] });
        if (!pricing) {
            throw new NotFoundError("PRICING_NOT_FOUND");
        }
        await pricing.event.onlyOwner(currentUser);

        if (data.title) {
            pricing.title = data.title;
        }
        if (data.price) {
            pricing.price = data.price;
        }

        await this.repository.save(pricing);

        return true;
    }

    async delete(id: number, currentUser: User) {

        if (!id) {
            throw new BadRequestError("MISSING_PARAMETER");
        }
        const pricing = await this.repository.findOne(id, { relations: ["event", "event.user"] });
        if (!pricing) {
            throw new NotFoundError("PRICING_NOT_FOUND");
        }
        await pricing.event.onlyOwner(currentUser);

        // Check if pricing is already linked to a ticket
        const tickets = await this.ticketRepository.find({ where: { pricing: pricing } });
        if (tickets.length) {
            throw new UnauthorizedError("PRICING_HAVE_TICKET")
        }

        await this.repository.delete(pricing);

        return true;
    }
}