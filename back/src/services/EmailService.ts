import { Service } from "typedi";
import { ContactRequest, ContactResponse } from "@shared/interfaces";
import { MajentusMail } from '../helpers/email/MajentusMail';
import { Email } from '../helpers/email/Mail';

@Service()
export class EmailService {
    async contact(data: ContactRequest): Promise<ContactResponse> {
        try {
            const mailToMajentus = new Email({
                to: 'contact@majentus.ch',
                subject: data.subject,
                content: data.message,
                from: {
                    name: data.name,
                    email: 'no-reply@majentus.ch'
                },
                replyTo: data.email
            })
            await mailToMajentus.send()

            if (data.sendCopy) {
                const mailFromMajentus = new MajentusMail({
                    to: data.email,
                    subject: data.subject,
                    content: `<p>Hello ${data.name}</p>, <p>Nous avons bien reçus votre message, nous y répondrons dans les plus bref délai</p><p>Votre message: <br/>${data.message}</p>`
                })
                await mailFromMajentus.send();
            }
            return {
                success: true
            }
        } catch (e) {
            console.log(e);
            return {
                success: false
            }
        }
    }
}