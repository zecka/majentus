import { UserRepository } from '../repository/UserRepository';
import { Service, Inject } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { User } from '../entity/User';
import { UnauthorizedError } from 'routing-controllers';
import { UpdateUserRequest, UserAvailabilityRequest, UserAvailabilityResponse } from '@shared/interfaces';
import { ChangeEmailService } from './ChangeEmailService';
import { UserValidator } from '../validators/UserValidator';
import { MajentusMail } from '../helpers/email/MajentusMail';
import { MetadataRepository } from '../repository/MetadataRepository';
import { nanoid } from 'nanoid';
import { Media } from '../entity/Media';
import { Repository } from 'typeorm';

@Service()
export class UserService {

    @InjectRepository(UserRepository)
    private repository: UserRepository;

    @InjectRepository(MetadataRepository)
    private metaDataRepo: MetadataRepository;

    @InjectRepository(Media) private mediaRepo: Repository<Media>

    @Inject()
    changeEmailService: ChangeEmailService

    async trash(id: number, currentUser: User) {
        let userToRemove = await this.repository.findOne(id);
        if (userToRemove.id !== currentUser.id && currentUser.role !== 'admin') {
            throw new UnauthorizedError();
        }
        return await this.repository.remove(userToRemove);
    }
    findOne(id: number) {
        return this.repository.findOne(id)
    }
    findAll() {
        return this.repository.findAll();
    }
    getBasicData(id: number): Promise<UpdateUserRequest> {
        return this.repository.getBasicData(id);
    }
    findEventsByUserId(id: number) {
        return this.repository.findEventsByUserId(id)
    }
    async availability(info: UserAvailabilityRequest): Promise<UserAvailabilityResponse> {
        return this.repository.availability(info)
    }
    async register(user: User) {
        user.role = "seller";
        await UserValidator.register(user);
        const savedUser = await this.repository.insert(user);
        user = await this.repository.findOne(savedUser.raw.insertId);
        await this.sendActivationCode(user)
        return user
    }
    async update(data: UpdateUserRequest, currentUser: User) {
        const user = await this.findOne(currentUser.id);
        if (data.password) {
            const valid = await user.checkPassword(data.oldPassword)
            if (!valid) {
                throw new UnauthorizedError("Current password is wrong");
            }
            user.setPassword(data.password);
        }

        if (data.firstName) {
            user.firstName = data.firstName;
        }
        if (data.lastName) {
            user.lastName = data.lastName;
        }
        if (data.email && data.email !== user.email) {
            await this.changeEmailService.changeEmailRequest(user, data.email);
        }
        if (data.avatar) {
            const media = await this.mediaRepo.findOne(data.avatar.id);
            user.avatar = media

        }
        const saved = await this.repository.save(user)
        return saved
    }

    async resendActivationCode(username: string) {
        const user = await this.repository.findOneOrFail({ where: { username: username } })
        return this.sendActivationCode(user);
    }
    async sendActivationCode(user: User): Promise<'success' | 'active'> {
        // Check if user is already active
        if (user.active) {
            return 'active';
        }
        // CHECK IF Activation Code already exist for this user
        const metaActivationCode = await this.metaDataRepo.findOne({ where: { key: "activationCode", entityName: "User", entityId: user.id } })
        if (metaActivationCode) {
            await this.activationCodeEmail(metaActivationCode.value, user);
        } else {
            // set activation code
            const activationCode = nanoid(48);
            this.metaDataRepo.set({
                entityId: user.id,
                entityName: "User",
                key: "activationCode",
                value: activationCode
            })
            await this.activationCodeEmail(activationCode, user);
        }


        return "success"
    }
    async activationCodeEmail(activationCode: string, user: User) {
        const activationLink = process.env.TYPEORM_FRONT_URL + "/login?activationCode=" + activationCode + '&user=' + user.username;
        const subject = "Activer votre compte";
        const button = await MajentusMail.button({ label: "Activer mon compte", href: activationLink })
        const content = `
            <h1>Bienvenu sur Majentus</h1>
            <p>Veuillez activer votre compte en cliquant sur le lien ci-dessous:</p>
            ${button}
        `
        const mail = new MajentusMail({
            subject,
            content,
            to: user.email
        })

        await mail.send();
    }

    async activation(activationCode: string, username: string): Promise<'success' | 'ACTIVATION_ERROR' | 'ACTIVATION_SUCCESS'> {
        const user = await this.repository.findOne({ where: { username: username } });
        if (!user) {
            return "ACTIVATION_ERROR";
        }
        if (user.active) {
            return "ACTIVATION_SUCCESS";
        }
        const metadata = await this.metaDataRepo.findOne({ where: { key: "activationCode", value: activationCode } })
        if (metadata) {
            await this.repository.update(metadata.entityId, { active: true });
            // this.metaDataRepo.delete(metadata)
            return "ACTIVATION_SUCCESS";
        }
        return "ACTIVATION_ERROR";
    }
}