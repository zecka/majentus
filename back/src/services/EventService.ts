import { Service } from "typedi";
import { EventRepository } from '../repository/EventRepository';
import { InjectRepository } from "typeorm-typedi-extensions";
import { EventStatus } from "@shared/interfaces";
import { User } from "../entity/User";
import { EventValidator } from "../validators/EventValidator";
import { Event } from "../entity/Event";
import { CategoryRepository } from '../repository/CategoryRepository';
import { NotFoundError } from "routing-controllers";
import { EventDateRepository } from '../repository/EventDateRepository';
import { TicketRepository } from '../repository/TicketRepository';
import { Ticket } from "../entity/Ticket";

@Service()
export class EventService {

    @InjectRepository(EventRepository)
    private repository: EventRepository;

    @InjectRepository(CategoryRepository)
    private categoryRepo: CategoryRepository;

    @InjectRepository(EventDateRepository)
    private eventDateRepo: EventDateRepository;

    @InjectRepository(TicketRepository)
    private ticketRepo: TicketRepository;

    findAll() {
        return this.repository.find({ relations: ["categories", "user", "cover"] });
    }
    // TODO: Type response
    async getOne(id: number): Promise<any> {
        try {
            return await this.repository.getOne(id)
        } catch (e) {
            throw new NotFoundError("Event not found")
        }
    }
    getDates(eventId: number) {
        return this.eventDateRepo.getByEventId(eventId)
    }
    getTickets(eventId: number): Promise<Ticket[]> {
        return this.ticketRepo.getByEventId(eventId);
    }
    async create(eventData: any, user: User) {
        eventData.status = EventStatus.Pending
        await EventValidator.insert(eventData)
        const event = new Event()
        event.generate(eventData)
        if (eventData.categories) {
            event.categories = await this.categoryRepo.findByIds(eventData.categories);
        }
        event.user = user;
        const savedEvent = await this.repository.save(event)
        return await this.getOne(savedEvent.id)
    }
    async update(eventData, currentUser: User) {
        await EventValidator.update(eventData, currentUser)
        const savedEvent = await this.repository.updateFromJson(eventData);
        return await this.getOne(savedEvent.id)
    }
    async delete(id: number, currentUser: User) {
        let eventToRemove = await this.repository.findOne(id, { relations: ['user'] });
        if (!eventToRemove) {
            throw new NotFoundError("this event doesnt exist")
        }
        await eventToRemove.onlyOwner(currentUser)
        return this.repository.remove(eventToRemove);
    }
    async currentUserEvents(currentUser: User) {
        return this.repository.find({ relations: ["categories", "dates"], where: { user: currentUser.id } });
    }
    async upcomings() {
        return this.repository.upcomings()
    }
    async pasts() {
        return this.repository.pasts()
    }

}