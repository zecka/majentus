import { Service, Inject } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { OrderRepository } from '../repository/OrderRepository';
import { OrderResponse, CreateTicketRequestData, EventStatus, PricingRequestData } from "@shared/interfaces";
import { UnauthorizedError, BadRequestError, NotFoundError } from "routing-controllers";
import { Ticket } from "../entity/Ticket";
import { Order } from "../entity/Order";
import { nanoid } from "nanoid";
import { TicketPrinter } from "../helpers/TicketPrinter";
import Archiver = require('archiver');
import { EventDateRepository } from '../repository/EventDateRepository';
import { EventDate } from "../entity/EventDate";
import { TicketRepository } from '../repository/TicketRepository';
import { TicketService } from './TicketService';
import { OrderMail } from "../helpers/email/OrderMail";
import { UserRepository } from '../repository/UserRepository';
import { GatewayRepository } from "../repository/GatewayRepository";

@Service()
export class OrderService {

    @InjectRepository(OrderRepository)
    private repository: OrderRepository;

    @InjectRepository(EventDateRepository)
    private eventDateRepository: EventDateRepository;

    @InjectRepository(TicketRepository)
    private ticketRepository: TicketRepository;

    @InjectRepository(GatewayRepository)
    private gatewayRepository: GatewayRepository;

    @InjectRepository(UserRepository)
    private userRepository: UserRepository;

    @Inject()
    private ticketService: TicketService

    public async getOne(orderId: number, orderToken: string): Promise<OrderResponse> {
        const order = await this.repository.getOne(orderId);
        if (orderToken !== order.token) {
            throw new UnauthorizedError("BAD_TOKEN");
        }
        return order;
    }

    public async getOneWhithoutToken(orderId: number): Promise<Order> {
        try {
            const order = await this.repository.getOne(orderId);
            return order;
        } catch (e) {
            console.log(e.name)
            throw new NotFoundError("ORDER_NOT_FOUND")
        }
    }

    public async checkIfTicketIsInOrder(orderId: number, ticketId: number, orderToken: string): Promise<number> {
        const order = await this.getOne(orderId, orderToken);
        const ticket = order.tickets.find((t) => t.id === ticketId);
        if (!ticket) {
            throw new BadRequestError("TICKET_NOT_IN_ORDER");
        }
        return ticket.id;
    }
    public async getAllTicketsIdFromOrder(orderId, orderToken): Promise<number[]> {
        const order = await this.getOne(orderId, orderToken);
        return order.tickets.map(t => t.id);
    }


    public async create(data: CreateTicketRequestData, chargeToken: string) {
        if (chargeToken === "" || !chargeToken) {
            throw new BadRequestError("NO_STRIPE_TOKEN")
        }
        // Find data relations entities
        await this.processRequestData(data);
        // Check required data is set in data var
        await this.checkPrerequisite(data);
        if (data.eventDate.status !== EventStatus.Open) {
            throw new BadRequestError("EVENTDATE_NOT_OPEN")
        }
        const tickets = await this.ticketService.createTicketsFromRequest(data);
        if (chargeToken !== "") {
            await this.chargeOrder(tickets, chargeToken, data);
        }
        // TODO: refund charge if save ticket fail
        // Maybe use payment intent instead of charge: https://stripe.com/docs/api/payment_intents/capture
        await this.ticketService.saveTickets(tickets);
        const order = await this.createWithTickets(tickets);
        const mail = new OrderMail(order);
        await mail.send();
        return order;
    }

    /**
 * Charge card with total ticket amount
 * Use Stripe api
 * @see https://stripe.com/docs/testing#cards-responses To test strip error response
 * @param tickets 
 * @param token 
 */
    async chargeOrder(tickets: Ticket[], token: string, data: CreateTicketRequestData) {

        const user = await this.userRepository.findOne(data.eventDate.event.user.id)
        const userGateway = await this.gatewayRepository.getUserGateways(user)
        const stripe = require("stripe")(userGateway.secretKey);

        let amount = 0;
        tickets.forEach(ticket => amount += ticket.pricing.price);
        // Stripe amount is centime so we need to to multiply by 100
        amount *= 100;
        const customerName = `${data.firstName} ${data.lastName}`
        // TODO: Use customer date in charge request
        var newCharge = {
            amount,
            currency: "CHF",
            source: token, // obtained with Stripe.js on the client side
            description: `Payé par: ${customerName}`,
            receipt_email: data.email,
            shipping: {
                name: customerName,
                address: data.address,
            },
        };
        try {
            const payment = await stripe.charges.create(newCharge);
            if (payment.status !== "succeeded") {
                throw new UnauthorizedError("Payment refused: Unknow error");
            }
        } catch (e) {
            throw new UnauthorizedError("Paiement refusé: " + e.message);
        }
    }

    /**
     * Create an save order from given tickets
     * Usage in ticket controller
     * @param tickets 
     */
    public async createWithTickets(tickets: Ticket[]): Promise<Order> {

        if (!tickets || !tickets.length) {
            throw new BadRequestError("ORDER_NEED_TICKET")
        }

        const ticket = tickets[0];
        const order = new Order();

        order.email = ticket.email;
        order.token = nanoid(48);
        order.tickets = tickets;

        await this.repository.save(order);
        return order;
    }
    public async zipOrder(orderId: number, orderToken: string): Promise<Archiver.Archiver> {
        const safeIds = await this.getAllTicketsIdFromOrder(orderId, orderToken);
        return TicketPrinter.createZip(safeIds)
    }
    public async ticketPdf(orderId: number, ticketId: number, orderToken: string): Promise<Buffer> {
        const safeTicketId = await this.checkIfTicketIsInOrder(orderId, ticketId, orderToken);
        return TicketPrinter.createPdf(safeTicketId);
    }

    /**
     * 
     */
    async processRequestData(data: CreateTicketRequestData) {
        data.eventDate = await this.eventDateRepository.findOne(data.eventDateId, { relations: ["event", "event.user"] });
    }

    async checkPrerequisite(data: CreateTicketRequestData) {
        if (!data.eventDate || !data.pricing) {
            throw new BadRequestError("You need provide evendate and pricing ids");
        }
        const eventDate = await this.eventDateRepository.findOne(data.eventDateId, { relations: ["event"] });
        if (!eventDate) {
            throw new BadRequestError("Event date not found");
        }
        if (!data.pricing.length) {
            throw new BadRequestError("You must select a pricing");
        }
        await this.checkAvailability(eventDate, data.pricing);
    }

    async checkAvailability(eventDate: EventDate, pricings: PricingRequestData[]) {
        const count = await this.ticketRepository.count({ where: { eventDate: eventDate.id } });
        const availability = eventDate.event.capacity - count;
        let totalQuantity = 0;
        pricings.forEach(pricing => {
            totalQuantity += Number(pricing.quantity);
        })
        if (totalQuantity > availability) {
            throw new UnauthorizedError("Cet événement n'a plus assez de place disponible, place restante: " + availability);
        }
    }
}