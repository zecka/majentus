import { User } from '../entity/User';
import { getCustomRepository, getConnectionManager } from 'typeorm';
import { ChangeEmailRepository } from '../repository/ChangeEmailRepository';
import { ChangeEmail } from '../entity/ChangeEmail';
import { nanoid } from 'nanoid';
import { MajentusMail } from '../helpers/email/MajentusMail';
import { UnauthorizedError, NotFoundError } from 'routing-controllers';
import { UserRepository } from '../repository/UserRepository';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class ChangeEmailService {

    @InjectRepository(ChangeEmailRepository)
    private repository: ChangeEmailRepository;

    @InjectRepository(UserRepository)
    private userRepository: UserRepository;

    async validateChangeEmail(id: number, token: string) {
        const emailChange = await this.repository.getOneWithToken(id);
        if (!emailChange || !id) {
            throw new NotFoundError("WRONG_ID_EMAIL_CHANGE");
        }
        if (emailChange.token !== token) {
            throw new UnauthorizedError("WRONG_TOKEN_EMAIL_CHANGE")
        }
        const user = await this.userRepository.findOneOrFail(emailChange.user.id);
        user.email = emailChange.email;
        await this.userRepository.save(user);
        await this.repository.delete(emailChange);
        return true;
    }
    async cancelChangeToken(id: number, token: string) {
        const emailChange = await this.repository.getOneWithCancelToken(id);
        if (!emailChange || !id) {
            throw new NotFoundError("WRONG_ID_EMAIL_CHANGE");
        }

        if (emailChange.cancelToken !== token) {
            throw new UnauthorizedError("WRONG_CANCEL_TOKEN_EMAIL_CHANGE")
        }
        await this.repository.delete(emailChange);
        return true

    }
    async changeEmailRequest(user: User, email: string): Promise<ChangeEmail> {
        // check if change email request already exist for this user
        const changeEmail = new ChangeEmail()
        const existingChangeEmail = await this.repository.getByUser(user);
        if (existingChangeEmail) {
            changeEmail.id = existingChangeEmail.id;
            changeEmail.email = existingChangeEmail.email;
            changeEmail.user = existingChangeEmail.user;
            changeEmail.token = existingChangeEmail.token;
            changeEmail.cancelToken = existingChangeEmail.cancelToken;
        } else {
            changeEmail.email = email;
            changeEmail.user = user;
            changeEmail.token = this.createToken();
            changeEmail.cancelToken = this.createToken();
        }
        const savedChangeEmail = await this.repository.save(changeEmail);
        await this.sendValidationEmail(savedChangeEmail);

        return this.repository.save(changeEmail);

    }
    async resendChangeEmailValidation(user: User) {
        const changeEmail = await this.repository.getByUser(user);
        return this.sendValidationEmail(changeEmail);
    }

    private createToken() {
        return nanoid(48);
    }
    private getChangeUrl(changeEmail: ChangeEmail): string {
        return process.env.TYPEORM_FRONT_URL + '/change-email/?id=' + changeEmail.id + '&token=' + changeEmail.token;
    }
    private async sendValidationEmail(changeEmail: ChangeEmail) {
        const url = this.getChangeUrl(changeEmail);
        let content = "<p>Cliquez sur le lien ci-dessous pour valider votre nouvelle adresse email:</p>";
        content += await MajentusMail.button({ label: "Confirmer mon adresse email", href: url })
        const mailer = new MajentusMail({
            to: changeEmail.email,
            content: content,
            subject: "Validez votre nouvel adresse email"
        })
        return mailer.send()
    }
}