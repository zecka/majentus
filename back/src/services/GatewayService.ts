import { GatewayRepository } from '../repository/GatewayRepository';
import { getConnectionManager } from 'typeorm';
import { StripeEntityRequest, GatewayInterface } from '@shared/interfaces';
import { User } from '../entity/User';
import { BadRequestError, NotFoundError, UnauthorizedError } from 'routing-controllers';
import { StripeAccount } from '../entity/StripeAccount';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';

@Service()
export class GatewayService {

    @InjectRepository(GatewayRepository)
    private repository: GatewayRepository;


    async updateStripe(data: StripeEntityRequest, currentUser: User) {
        if (!data.secretKey || !data.publicKey || !data.id) {
            throw new BadRequestError("MISSING_PARAMETERS")
        }
        const stripeAccount = await this.repository.findOne(data.id, { relations: ["user"] });
        if (!stripeAccount) {
            throw new NotFoundError("GATEWAY_NOT_FOUND")
        }
        if (stripeAccount.user.id !== currentUser.id) {
            throw new UnauthorizedError()
        }
        stripeAccount.secretKey = data.secretKey
        stripeAccount.publicKey = data.publicKey

        return this.repository.save(stripeAccount);
    }


    async createStripeAccount(data: StripeEntityRequest, currentUser: User) {
        if (!data.secretKey || !data.publicKey) {
            throw new BadRequestError("MISSING_PARAMETERS")
        }
        const stripeAccount = new StripeAccount();
        stripeAccount.user = currentUser;
        stripeAccount.secretKey = data.secretKey;
        stripeAccount.publicKey = data.publicKey;
        return this.repository.insert(stripeAccount);
    }
    async getUserGateways(currentUser: User): Promise<GatewayInterface | null> {
        const gateway = await this.repository.getUserGateways(currentUser);
        if (gateway.id === 0) {
            return null
        } else {
            return gateway;
        }
    }
}
