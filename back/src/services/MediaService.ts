import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Media } from '../entity/Media';
import { Repository } from "typeorm";
import { User } from '../entity/User';
import { NotFoundError, UnauthorizedError } from "routing-controllers";

@Service()
export class MediaService {
    @InjectRepository(Media) private repository: Repository<Media>

    findAll() {
        return this.repository.find({ relations: ["user"] });
    }

    create(image: any, user: User) {
        const media = new Media(image);
        if (user) {
            media.user = user
        }
        return this.repository.save(media)
    }
    async delete(id: number, user: User) {
        let mediaToRemove = await this.repository.findOne(id, { relations: ["user"] });
        if (!mediaToRemove) {
            throw new NotFoundError("Media " + id + " doesn't exist")
        }
        if (!mediaToRemove.user || mediaToRemove.user.id === (user ? user.id : false)) {
            return await this.repository.remove(mediaToRemove);
        } else {
            throw new UnauthorizedError("Your are not allowed to delete this media")
        }
    }
}