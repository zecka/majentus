import { getCustomRepository, In } from 'typeorm';
import { EventDate } from '../entity/EventDate';
import { EventDateRepository } from '../repository/EventDateRepository';
import { TicketRepository } from '../repository/TicketRepository';
import { Ticket } from '../entity/Ticket';
import * as _ from "lodash";
import { EventRepository } from '../repository/EventRepository';
import { EventDatePricingStats } from '@shared/interfaces/StatsInterfaces';
import { Service } from 'typedi';
import { InjectRepository } from "typeorm-typedi-extensions";

@Service()
export class StatService {

    @InjectRepository(EventDateRepository)
    private eventDateRepo: EventDateRepository;

    @InjectRepository(TicketRepository)
    private ticketRepository: TicketRepository;

    @InjectRepository(EventRepository)
    private eventRepository: EventRepository;

    async getSellerMonthlySells(sellerId: number, month: number) {
        const dates = await this.eventDateRepo.getBySellerId(sellerId);
        if (!dates.length) {
            return []
        }
        const tickets = await this.ticketRepository.find({
            where: {
                eventDate: In(dates.map(date => date.id)),
            },
            order: {
                createdAt: "ASC"
            },
            relations: ["pricing"]
        })
        var orderedByMonths = _.groupBy(tickets, function (element: Ticket) {
            return element.createdAt.toISOString().substring(0, 7);
        });
        return orderedByMonths
    }

    async getEventDatesPricingStats(userId: number): Promise<EventDatePricingStats[]> {

        const dates = await this.eventDateRepo.getBySellerId(userId);
        if (!dates.length) {
            return []
        }
        const tickets = await this.ticketRepository.find({
            where: { eventDate: In(dates.map(date => date.id)) },
            relations: ["pricing", "eventDate", "eventDate.event"],
        })

        const stats: EventDatePricingStats[] = [];


        tickets.forEach((ticket, index) => {
            const statFinded = stats.find(stat => stat.id === ticket.eventDate.id);
            if (statFinded) {
                const itemFinded = statFinded.items.find(pricing => pricing.id === ticket.pricing.id)
                if (itemFinded) {
                    itemFinded.value++;
                    statFinded.items[0].value--;
                } else {
                    statFinded.items[0].value--;
                    statFinded.items.push({ id: ticket.pricing.id, price: ticket.pricing.price, name: ticket.pricing.title, value: 1 })
                }
            } else {
                stats.push({
                    id: ticket.eventDate.id,
                    name: ticket.eventDate.event.title,
                    date: ticket.eventDate.date,
                    capacity: ticket.eventDate.event.capacity,
                    items: [
                        { id: 0, name: "Disponible", price: 0, value: ticket.eventDate.event.capacity - 1 },
                        { id: ticket.pricing.id, price: ticket.pricing.price, name: ticket.pricing.title, value: 1 }
                    ]
                })
            }
        })

        return stats;
    }
}