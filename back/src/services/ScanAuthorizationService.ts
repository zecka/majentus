import { EventDateRepository } from "../repository/EventDateRepository";
import { User } from "../entity/User";
import { NotFoundError, UnauthorizedError, BadRequestError, ForbiddenError } from 'routing-controllers';
import { ScanAuthorizationInterface, EventDateInterface } from '@shared/interfaces'
import { ScanAuthorization } from '../entity/ScanAuthorization';
import { ScanAuthorizationRepository } from "../repository/ScanAuthorizationRepository";
import { nanoid } from "nanoid";
import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Ticket } from '../entity/Ticket';
import { Event } from '../entity/Event';

@Service()
export class ScanAuthorizationService {

    @InjectRepository(EventDateRepository)
    private eventDateRepository: EventDateRepository;

    @InjectRepository(ScanAuthorizationRepository)
    private repository: ScanAuthorizationRepository;

    async getByToken(token: string): Promise<ScanAuthorization> {
        return this.repository.getByToken(token);
    }
    async getDateAuthorization(eventDateId: number, currentUser: User): Promise<ScanAuthorization> {
        const eventDate = await this.eventDateRepository.findOneOrFail(eventDateId, { relations: ["event", "event.user", "scanAuthorization"] })
        if (!eventDate) {
            throw new NotFoundError("EVENT_DATE_NOT_FOUND");
        }
        if (!currentUser || eventDate.event.user.id !== currentUser.id) {
            throw new UnauthorizedError("NOT_ALLOWED")
        }
        if (!eventDate.scanAuthorization || !eventDate.scanAuthorization) {
            return null;
        }
        return this.repository.getOne(eventDate.scanAuthorization.id);
    }
    async createDateAuthorization(eventDateId: number, currentUser: User): Promise<ScanAuthorizationInterface> {
        const eventDate = await this.eventDateRepository.findOneOrFail(eventDateId, { relations: ["event", "event.user", "scanAuthorization"] })
        if (!eventDate) {
            throw new NotFoundError("EVENT_DATE_NOT_FOUND");
        }
        if (!currentUser || eventDate.event.user.id !== currentUser.id) {
            throw new UnauthorizedError("NOT_ALLOWED")
        }
        if (eventDate.scanAuthorization) {
            return eventDate.scanAuthorization;
        }

        const scanAuthorization = new ScanAuthorization()
        const token = nanoid(48);
        scanAuthorization.user = currentUser;
        scanAuthorization.eventDate = eventDate;
        scanAuthorization.token = token;
        await this.repository.insert(scanAuthorization)

        return {
            id: scanAuthorization.id,
            token: token,
            createdAt: scanAuthorization.createdAt,
            eventDate: eventDate
        }
    }
    async getUserAuthorizations(user: User): Promise<ScanAuthorization[]> {
        if (!user) {
            throw new UnauthorizedError("NOT_ALLOWED")
        }
        return this.repository.getByUser(user);
    }

    async deleteAuthorization(id: number, currentUser: User): Promise<boolean> {
        const authorization = await this.repository.findOne(id, { relations: ["user"] });
        if (authorization.user.id !== currentUser.id) {
            throw new UnauthorizedError("NOT_ALLOWED");
        }
        try {
            await this.repository.delete(authorization.id)
            return true
        } catch (e) {
            throw new ForbiddenError("UNKNOW_ERROR");
        }
    }

    async checkIfIsAllowToScan(ticket: Ticket, authorization: string, user: User) {
        const eventTicket: Event = ticket.eventDate.event;
        const scanAuthorization = await this.repository.getByDateID(ticket.eventDate.id)

        if (!scanAuthorization) {
            throw new UnauthorizedError("NO_AUTHORIZATION_FOUND");
        }

        if (authorization !== scanAuthorization.token) {
            throw new UnauthorizedError("NOT_ALLOW_TO_SCAN");
        }
        return true

    }

}