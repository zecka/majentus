import { EventSubscriber } from 'typeorm';
import { EntitySubscriberInterface } from 'typeorm';
import { Event } from '../entity/Event';
import { InsertEvent } from 'typeorm';
import { LoadEvent } from 'typeorm/subscriber/event/LoadEvent';
import { EventDate } from '../entity/EventDate';
import { EventDateRepository } from '../repository/EventDateRepository';
@EventSubscriber()
export class MediaSubscriber implements EntitySubscriberInterface<Event> {
    listenTo() {
        return Event;
    }
    beforeInsert(event: InsertEvent<Event>) {
        event.entity.genereateSecret();
    }

    async afterLoad(eventEntity: Event, event: LoadEvent<Event>) {
        if (eventEntity.id) {
            const eventDates: EventDate[] = await event.manager.getRepository(EventDate).find({ where: { event: eventEntity.id } })
            if (eventDates && eventDates.length) {
                // Order ASC: Get the oldest date
                eventDates.sort((a, b) => a.date.getTime() - b.date.getTime())
                const firstEventDate: Date = new Date(eventDates[0].date);

                // Order DESC: get the latest date
                eventDates.sort((a, b) => b.date.getTime() - a.date.getTime())
                const lastEventDate: Date = new Date(eventDates[0].date);

                // Add his own duration to lastest date
                // lastEventDate.setMinutes(lastEventDate.getMinutes() + eventDates[0].duration);
                eventEntity.start = firstEventDate;
                eventEntity.end = lastEventDate;
            }
        }
    }
}