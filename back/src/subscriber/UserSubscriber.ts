import { EventSubscriber, EntitySubscriberInterface, InsertEvent, getRepository, getConnection, UpdateEvent } from "typeorm";
import { NotFoundError } from 'routing-controllers';
import { User } from "../entity/User";
import { UserValidator } from '../validators/UserValidator';
import MediaHelpers from '../helpers/MediaHelpers';
import { Media } from "../entity/Media";

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
    listenTo() {
        return User;
    }
    async beforeInsert(event: InsertEvent<User>) {
        await UserValidator.register(event.entity)
        event.entity.hashPassword();
    }
    async beforeUpdate(event: UpdateEvent<User>) {
        await UserValidator.update(event.entity);
    }

    async afterInsert(event: InsertEvent<User>) {
        await MediaHelpers.setUserMedia(event.entity.avatar, event.entity);
    }
    async afterUpdate(event: UpdateEvent<User>) {
        if (event.entity && event.entity.avatar) {
            await MediaHelpers.setUserMedia(event.entity.avatar.id, event.entity);
            const oldAvatar = event.databaseEntity.avatar;
            const newAvatar = event.entity.avatar;
            if (oldAvatar && oldAvatar.id && oldAvatar.id !== newAvatar.id) {
                event.manager.getRepository(Media).delete(oldAvatar.id);
            }
        }
    }

}