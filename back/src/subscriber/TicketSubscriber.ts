import { EventSubscriber, EntitySubscriberInterface, InsertEvent, getRepository, getConnection, UpdateEvent } from "typeorm";
import { Ticket } from "../entity/Ticket";
import TicketToken from '../helpers/TicketToken';
import { TicketRepository } from '../repository/TicketRepository';

@EventSubscriber()
export class TicketSubscriber implements EntitySubscriberInterface<Ticket> {
    listenTo() {
        return Ticket;
    }

    async afterInsert(ticket: InsertEvent<Ticket>) {
        const encryptor = new TicketToken(ticket.entity);
        ticket.entity.token = await encryptor.generate()
        await ticket.manager.getCustomRepository(TicketRepository).save(ticket.entity)
        return true;
    }

}