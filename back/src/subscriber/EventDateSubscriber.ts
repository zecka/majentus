import { EventSubscriber, EntitySubscriberInterface, InsertEvent, getRepository, getConnection, UpdateEvent } from "typeorm";
import { EventDate } from "../entity/EventDate";
import { Event } from "../entity/Event";
import { NotFoundError } from 'routing-controllers';
import { LoadEvent } from "typeorm/subscriber/event/LoadEvent";
import { Ticket } from '../entity/Ticket';

@EventSubscriber()
export class EventDateSubscriber implements EntitySubscriberInterface<EventDate> {

    listenTo() {
        return EventDate;
    }
    async afterLoad(eventDateEntity: EventDate, event: LoadEvent<EventDate>) {
        const ticketRepo = event.manager.getRepository(Ticket)
        eventDateEntity.sold = await ticketRepo.count({ where: { eventDate: eventDateEntity.id } });
        return true
    }

}