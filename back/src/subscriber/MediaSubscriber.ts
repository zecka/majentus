import { EventSubscriber, EntitySubscriberInterface, InsertEvent, getRepository, getConnection, UpdateEvent, RemoveEvent } from "typeorm";
import { NotFoundError } from 'routing-controllers';
import { User } from "../entity/User";
import { UserValidator } from '../validators/UserValidator';
import MediaHelpers from '../helpers/MediaHelpers';
import { Media } from "../entity/Media";
import { FileUploader } from '../helpers/FileUpload';

@EventSubscriber()
export class MediaSubscriber implements EntitySubscriberInterface<Media> {
    listenTo() {
        return Media;
    }
    afterRemove(event: RemoveEvent<Media>) {
        if (event.entity) {
            FileUploader.delete(event.entity.path);
        }
    }
}