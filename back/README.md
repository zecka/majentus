# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

## Ressources utiles:

1. TypeORM + Routing Controller + typedi https://github.com/ktanakaj/typeorm-example (Attention, certaine decorateur sont outdated) [Voir ici](https://github.com/typeorm/typeorm-typedi-extensions/commit/2a824e3841e28e5988728762e11342663c025a23)
