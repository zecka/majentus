echo $HOME

# $1 is $npm_config_prod_path
node -v
PATH="$HOME/.nvm/versions/node/v12.18.1/bin:$PATH"
echo $1
cd "$1/back"
npm i
echo "INSTALL BACK FINISH"
cd "$1/front"
npm i
npm run build
echo "RUN BUILD FINISH"
pm2 stop "front-$2"
pm2 stop "back-$2"
pm2 restart "front-$2"
pm2 restart "back-$2"