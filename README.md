# Installation en local

## Prérequis

-   NodeJS v12.14.0 : [Télécharger NodeJS](https://nodejs.org/download/release/v12.14.0/)
-   [Visual Studio Code](https://code.visualstudio.com/)
-   [Veture (extension VScode)](https://marketplace.visualstudio.com/items?itemName=octref.vetur)

## Base de donnée

Créer une base de donnée mysql, par exemple sur MAMP

## Back End

### Variable d'environnement

1. Renomé le fichier `back/env/.env.example` en `.env`
2. Adaptez les variables relative à la base de donnée avec vos credentials (si base de donnée sur mamp avec les config par default, modifiez uniquement la variable `TYPEORM_DB_NAME`)
3. Créer un compte stripe, puis allez dans `dashboard->developers->Clés API->Clés standard`, Copié la clef publique dans la variable `STRIPE_PK` et la clef secrète dans la variable `STRIPE_SK`
4. Pas besoin de modifier les variables suivantes:

```sh
TYPEORM_DB_SYNC = 1
TYPEORM_DB_LOG  = 0
TYPEORM_DB_PORT = "8889"
TYPEORM_EXPRESS_PORT = "3000"
TYPEORM_FRONT_URL = "http://localhost:8000"
TYPEORM_BACK_URL = "http://localhost:3000"
TYPEORM_JWT_SECRET = "Lak91jnqjciqnf92"
```

### Installation et execution du serveur back

Depuis le terminal éxectuer les commandes suivantes dans le dossier `./back`

```sh
# Installation des dépendances
npm install
# Insertion de donnée fictive en base de donnée
npm run seed
# Démarrer le serveur
npm run start
```

Si vous avez une erreur à ce stade vous avez surement du vous tromper dans vos variables d'environnements

## Front

### Variable d'environnement front

1. Renomé le fichier `front/env/.env.example` en `.env`
2. Insérez votre clef publique stripe dans la variable `NUXT_STRIPE_PK`
3. Pas besoin de modifier les autre variables

### Installation et execution du serveur front

Depuis le terminal éxectuer les commandes suivantes dans le dossier `./front`

```sh
# Installation des dépendances
npm install
# Démarrer le serveur de développement
npm run dev
```

le site devrais désormais être accessible sur l'url suivante: http://localhost:8000/

## Developpement

Pour la revue de code je vous suggère d'ouvrir le dossier `front` et `back` dans des fenêtres différentes sur [Visual Studio Code](https://code.visualstudio.com/).

# Installation sur infomaniak :rocket:

[INFOMANIAK.MD](./INFOMANIAK.md)
