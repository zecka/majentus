import path from 'path'
import fs from 'fs'
// const routes = require('./routes/router.js')
const { getEnv } = require('./inc/env.js')
const { welcomeQr } = require('./inc/welcomeQr')
const env = getEnv()
welcomeQr(env)

export default {
  mode: 'universal',
  server: {
    host: process.env.NUXT_HOST,
    port: process.env.NUXT_PORT
  },
  env: env,
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    script: [
      {
        src: 'https://js.stripe.com/v3/',
        type: 'text/javascript'
      }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#9733ee' },
  /*
   ** Global CSS
   */
  css: [{ src: '~assets/scss/styles.scss', lang: 'scss' }],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/vuelidate',
    '@/plugins/api/api',
    '@/plugins/helpers/mj',
    '@/plugins/init',
    '@/plugins/components/vue-fragment',
    '@/plugins/components/mj-form',
    { src: '@/plugins/components/vue-qrcode-reader', ssr: false },
    { src: '@/plugins/components/vue-password-strength-metter', ssr: false },
    { src: '@/plugins/v-stripe-elements', ssr: false },
    { src: '@/plugins/am-charts', ssr: false }
  ],

  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxtjs/router', '@nuxt/typescript-build', 'nuxt-typed-vuex', '@nuxtjs/moment'],
  moment: {
    defaultLocale: 'fr',
    locales: ['fr']
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/pwa',
    '@nuxtjs/vuetify',
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'nuxt-webfontloader',
    'portal-vue/nuxt'
  ],
  pwa: {
    icon: {
      source: './static/icon.png'
    },
    meta: {
      name: 'Majentus'
    }
  },
  // Add the following:
  optionsPath: './vuetify.options.js',
  styleResources: {
    scss: ['./assets/scss/utils/*.scss']
  },

  auth: {
    router: {
      middleware: ['auth']
    },
    redirect: {
      login: '/login',
      logout: '/',
      callback: false,
      home: '/',
      dashboard: '/dashboard'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: process.env.NUXT_BACK_URL + '/auth/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: {
            url: process.env.NUXT_BACK_URL + '/auth/logout',
            method: 'post'
          },
          user: {
            url: process.env.NUXT_BACK_URL + '/auth/user',
            method: 'get',
            propertyName: 'user'
          }
        }
        // tokenRequired: true,
        // tokenType: 'bearer',
        // globalToken: true,
        // autoFetchUser: true
      }
    }
  },
  vuetify: {
    theme: {
      light: true,
      dark: false,
      themes: {
        light: {
          primary: '#9733EE'
          // secondary: '#ff8c00',
          // tertiary: '#4682bf',
          // accent: '#9c27b0'
        },
        dark: {
          primary: '#9733EE'
        }
      }
    },
    treeShake: true,
    customVariables: ['./assets/scss/theme/vuetify-variables.scss'],
    options: {
      customProperties: true
    },
    defaultAssets: {
      font: {
        family: 'Poppins'
      }
    }
  },
  webfontloader: {
    // add Google Fonts as "custom" | workaround required
    custom: {
      families: ['Poppins:n2,n4,n7,n9'],
      urls: [
        // for each Google Fonts add url + options you want
        // here add font-display option
        'https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;500;600;700;900&display=swap'
      ]
    }
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    transpile: [/typed-vuex/],
    extractCSS: process.env.NODE_ENV === 'production',
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
