// TODO: Use correct type
export const getTotal = (pricings: any) => {
  let total = 0
  pricings.forEach((element: any) => {
    if (element.quantity) {
      total += element.quantity * element.price
    }
  })
  return total
}
