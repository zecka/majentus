export enum EventStatus {
  Pending = "pending",
  Open = "open",
  Cancel = "cancel",
  Closed = "closed"
}
