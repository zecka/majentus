import { Vue, Prop, Inject } from 'nuxt-property-decorator'

export class CustomFieldProps extends Vue {
  @Prop()
  label?: string

  @Prop({ default: 'inline' })
  layout?: 'material' | 'inline' | 'block'
}

export default class FieldBase extends CustomFieldProps {


  @Prop({ default: null })
  placeholder?: string

  @Prop()
  value?: string

}
