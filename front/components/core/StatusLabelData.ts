import { EventStatus } from '../../../shared/interfaces'
export type StatusInterface = { value: string; label: string }
export const statusList: StatusInterface[] = [
  { value: EventStatus.Pending, label: 'En attente' },
  { value: EventStatus.Open, label: 'Ouvert' },
  { value: EventStatus.Closed, label: 'Fermé' },
  { value: EventStatus.Cancel, label: 'Annulé' }
]
