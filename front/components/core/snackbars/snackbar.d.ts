type SnackbarColor = 'warning' | 'success' | 'error'
interface Snackbar {
  active: boolean
  color: SnackbarColor
  timeout: number
  content: string
}
