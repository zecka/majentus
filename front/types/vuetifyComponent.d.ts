type VuetifyDataTableHeader = {
  text: string
  value: string
  align?: 'start' | 'center' | 'end'
  sortable?: boolean
  filterable?: boolean
  groupable?: boolean
  divider?: boolean
  class?: string | string[]
  width?: string | number
  filter?: (value: any, search: string, item: any) => boolean
  sort?: (a: any, b: any) => number
}

declare module 'v-stripe-elements/lib/index'

declare module 'dr-vue-echarts'
declare module 'chart.js'
declare module 'vuejs-clipper/src/clipper'
declare module 'color'
