type LoginAlertInterface = {
  type: string
  value: string
  username?: string
}
