declare module 'vue-stripe-checkout' {
  export class StripeCheckout extends Vue {
    pk: string;
    items: [];
    successUrl: string;
    cancelUrl: string;
    submitType: string;
    billingAddressCollection: string;
    clientReferenceId: string;
    customerEmail: string;
    sessionId: string;
    locale: string;

    redirectToCheckout(): void;
  }

  export class StripeElements extends Vue {
    pk: string;
    amount: Number;
    stripeAccount: string;
    apiVersion: string;
    locale: string;

    submit(): HTMLElement;
  }
}
