import Vue from 'vue'
import { Store } from 'vuex'
import { Framework } from 'vuetify';
import { IApi } from "@/plugins/api/index"
// ComponentOptions is declared in types/options.d.ts

declare module 'vue/types/options' {
  interface NuxtContext<V extends Vue> {
    app: V
    isClient: boolean
    isServer: boolean
    isStatic: boolean
    isDev: boolean
    store: Store<any> // Consider vuex-typex in future
    env: object
    params: object
    query: object
    $dialog: any
    $loading: any
    $modal: any
    $snackbar: any
    $toast: any
  }

  interface ComponentOptions<V extends Vue> {
    asyncData?(context: NuxtContext<V>): Promise<object> | object
    fetch?(context: NuxtContext<V>): Promise<object> | object
  }
}
declare module '@nuxt/types' {
  interface Context {
    $vuetify: Framework,
    $api: IApi,
    $am4core: any
  }
}

declare module 'vue-password-strength-meter' {
  interface VuePasswordStrengthMeter {

  }
}

declare module 'vue/types/vue' {
  interface Vue {
    asyncData?(context: NuxtContext<V>): Promise<object> | object
  }
}
