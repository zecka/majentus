import Vue from 'vue'
import Router from 'vue-router'

import Homepage from '~/routes/index'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Homepage
      },
      {
        name: 'login',
        path: '/login',
        component: () => import('./routes/login.vue').then((m) => m.default || m),
        props: true
      },
      {
        name: 'activation',
        path: '/login/:activationCode',
        component: () => import('./routes/login.vue').then((m) => m.default || m)
      },
      {
        name: 'register',
        path: '/register',
        component: () => import('./routes/register.vue').then((m) => m.default || m)
      },
      {
        name: 'test',
        path: '/test',
        component: () => import('./routes/test.vue').then((m) => m.default || m)
      },

      {
        name: 'styleguide',
        path: '/styleguide',
        component: () => import('./routes/styleguide.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboard',
        path: '/dashboard',
        component: () => import('./routes/dashboard/dashboard.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardNewEvent',
        path: '/dashboard/new-event',
        component: () => import('./routes/dashboard/event/new-event.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardEditEvent',
        path: '/dashboard/event/edit/:id',
        component: () => import('./routes/dashboard/event/edit-event.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardListEvent',
        path: '/dashboard/event/',
        component: () => import('./routes/dashboard/event/list-event.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardListTicket',
        path: '/dashboard/ticket/',
        component: () => import('./routes/dashboard/ticket/list-ticket.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardNewTicket',
        path: '/dashboard/create-order',
        component: () => import('./routes/dashboard/ticket/new-tickets.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardScanner',
        path: '/dashboard/scanner',
        component: () => import('./routes/dashboard/dashboard-scanner.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardAccount',
        path: '/dashboard/account',
        component: () => import('./routes/dashboard/account/account-settings.vue').then((m) => m.default || m)
      },
      {
        name: 'dashboardGateway',
        path: '/dashboard/gateway',
        component: () => import('./routes/dashboard/account/gateway-settings.vue').then((m) => m.default || m)
      },
      {
        name: 'validateEmailChange',
        path: '/change-email/',
        component: () =>
          import(/* webpackChunkName: "validations" */ './routes/validation.vue').then((m) => m.default || m)
      },
      {
        name: 'events',
        path: '/events',
        component: () =>
          import(/* webpackChunkName: "events" */ './routes/events/index.vue').then((m) => m.default || m)
      },
      {
        name: 'singleevent',
        path: '/events/:id',
        component: () =>
          import(/* webpackChunkName: "singleevent" */ './routes/events/SingleEvent.vue').then((m) => m.default || m)
      },
      {
        name: 'singleeventdate',
        path: '/events/:id/:date',
        component: () =>
          import(/* webpackChunkName: "singleevent" */ './routes/events/SingleEvent.vue').then((m) => m.default || m)
      },
      {
        name: 'iframeEvent',
        path: '/iframe/:id/',
        component: () =>
          import(/* webpackChunkName: "iframe" */ './routes/events/iframe.vue').then((m) => m.default || m)
      },
      {
        name: 'order',
        path: '/order/:id/:token',
        component: () => import(/* webpackChunkName: "order" */ './routes/order/order.vue').then((m) => m.default || m)
      },
      {
        name: 'scanner',
        path: '/scanner/:authorization',
        component: () => import('./routes/scanner.vue').then((m) => m.default || m)
      },
      {
        name: 'termsAndConditions',
        path: '/conditions-generales',
        component: () => import('./routes/terms-and-conditions.vue').then((m) => m.default || m)
      },
      {
        name: 'about',
        path: '/about',
        component: () => import(/* webpackChunkName: "about" */ './routes/about.vue').then((m) => m.default || m)
      },
      {
        name: 'pricing',
        path: '/tarifs',
        component: () => import(/* webpackChunkName: "pricing" */ './routes/pricing.vue').then((m) => m.default || m)
      },
      {
        name: 'contact',
        path: '/contact',
        component: () => import(/* webpackChunkName: "contact" */ './routes/contact.vue').then((m) => m.default || m)
      }
    ]
  })
}
