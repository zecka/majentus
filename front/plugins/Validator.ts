const required = (label: string) => (v: any) => !!v || `${label} est requis`;
const requiredCheck = (label: string) => (v: boolean) => v === true || (label !== 'Ce champ' ? label : `${label} est requis`);
const maxLength = (length: number) => (label: string) => (v: any) => (v && v.length <= length) || `${label} doit contenir au maximum ${length} charactères`
const minLength = (length: number) => (label: string) => (v: any) => (v && v.length >= length) || `${label} doit contenir au minimum ${length} charactères`
const min = (length: number) => (label: string) => (v: any) => (v && v >= length) || `${label} doit être au minimum ${length}`
const max = (length: number) => (label: string) => (v: any) => (v && v <= length) || `${label} doit être au maximum  ${length} `
const email = (label: string) => (v: any) => /.+@.+\..+/.test(v) || `Email invalide`
const username = (label: string) => (v: any) => /^[A-Za-z0-9\._-]+$/.test(v) || `Charactères autorisés : A-Z a-z 0-9 . _ -`



export const rules = (rulesFunctions: any[], label?: string) => {
  return rulesFunctions.map(rule => rule(label ? label : 'Ce champ'))
}
export const validators = {
  rules,
  required,
  requiredCheck,
  min,
  max,
  maxLength,
  minLength,
  email,
  username
}
export default validators;
