import { Context } from "@nuxt/types/app/index.d"
import { FetchApi } from "./fetch-api";
import { EventItemInterface, NewEventInterface } from "@shared/interfaces/EventItemInterface";
import { EditEventInterface } from '../../../shared/interfaces/EventItemInterface';

export class CategoryService extends FetchApi {
  async getAll(): Promise<any[]> {
    const categories = await this.get(this.backUrl + "/categories/")
    return categories.data;
  }

}
