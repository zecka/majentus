
import { EventService } from './event-service';
import { EventDateService } from './eventdate-service';
import { CategoryService } from './category-service';
import { TicketService } from './ticket-service';
import { UploadService } from './upload-service';
import { UserService } from './user-service';
import { OrderService } from './order-service';
import { StatsService } from './stats-service';
import { ScanService } from './scan-service';
import { MjContactService } from './contact-service';
export interface IFetchApi {
  axios: any;
  get(url: string): Promise<AxiosResponse<any>>;
  post(url: string, data?: Object): Promise<AxiosResponse<any>>;
  put(url: string, data?: Object): Promise<AxiosResponse<any>>;
  delete(url: string, data?: Object): Promise<AxiosResponse<any>>;
  getBlob(url: string): Promise<AxiosResponse<any>>;
}


export interface IApi {
  event: EventService
  user: UserService
  eventDate: EventDateService
  category: CategoryService,
  ticket: TicketService,
  upload: UploadService,
  order: OrderService,
  stats: StatsService,
  request: IFetchApi,
  scan: ScanService,
  contact: MjContactService
}
