
import { FetchApi } from './fetch-api';
import { ContactResponse, ContactRequest } from '~/../shared/interfaces';
export class MjContactService extends FetchApi {
  async send(data: ContactRequest): Promise<ContactResponse> {
    const response = await this.post(this.backUrl + "/contact/", data)
    return response.data;
  }
}
