import { FetchApi } from "./fetch-api";
import { NewUserRequestData, UserAvailabilityRequest, UserAvailabilityResponse, UpdateUserRequest } from '@shared/interfaces/RequestDataInterface';
import { GatewayInterface } from "~/../shared/interfaces";
import { StripeEntityRequest } from '../../../shared/interfaces/RequestDataInterface';
export class UserService extends FetchApi {
  async register(user: NewUserRequestData) {
    const result = await this.post(this.backUrl + '/users/register', user)
    return result;
  }
  async availability(data: UserAvailabilityRequest): Promise<UserAvailabilityResponse> {
    const result = await this.post(this.backUrl + '/users/availability', data)
    return result!.data;
  }
  async activation(activationCode: string, username: string): Promise<string> {
    const response = await this.get(this.backUrl + `/users/activation/${username}/${activationCode}`)
    return (response.data) ? response.data : "";
  }
  async resendActivation(username: string): Promise<string> {
    const response = await this.get(this.backUrl + "/users/resend-activation/" + username)
    return (response.data) ? response.data : "";
  }
  async getBasicInformation() {
    const response = await this.get(this.backUrl + "/users/update/basic")
    return response.data;
  }
  async update(updateData: UpdateUserRequest) {
    const response = await this.put(this.backUrl + "/users/update", updateData)
    return response.data;
  }
  async cancelChangeEmail(id: number, token: string) {
    const response = await this.get(this.backUrl + `/change-email/cancel/${id}/${token}`)
    return response.data;
  }
  async validateChangeEmail(id: number, token: string) {
    const response = await this.get(this.backUrl + `/change-email/validate/${id}/${token}`)
    return response.data;
  }
  async getGateway(): Promise<GatewayInterface | null> {
    const response = await this.get(this.backUrl + '/gateways/current')
    return response.data;
  }
  async createStripeAccount(data: StripeEntityRequest) {
    const response = await this.post(this.backUrl + '/gateways/stripe', data);
    return response.data;
  }
  async updateStripeAccount(data: StripeEntityRequest) {
    const response = await this.put(this.backUrl + '/gateways/stripe', data);
    return response.data;
  }
}
