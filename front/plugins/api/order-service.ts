import { FetchApi } from "./fetch-api";
import { OrderResponse } from '@shared/interfaces';

export class OrderService extends FetchApi {

  async getData(orderId: number, orderToken: string): Promise<OrderResponse> {
    const order = await this.get(this.backUrl + "/orders/" + orderId + "/" + orderToken);
    return order.data;
  }
  async zip(orderId: number, orderToken: string): Promise<void> {
    const url = this.backUrl + "/orders/zip/" + orderId + "/" + orderToken
    const response = await this.getBlob(url)
    saveAs(new Blob([response.data]), 'tickets.zip')
  }
  async pdf(orderId: number, ticketId: number, orderToken: string) {
    const url = this.backUrl + `/orders/pdf/${orderId}/${ticketId}/${orderToken}`
    const response = await this.getBlob(url)
    saveAs(new Blob([response.data]), 'ticket.pdf')
  }

}
