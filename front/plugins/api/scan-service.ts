import { FetchApi } from "./fetch-api";
import { ScanAuthorizationInterface, NewAuthorizationRequest, EventDateInterface } from '@shared/interfaces';
export class ScanService extends FetchApi {

  async getDateAuthorization(dateId: number): Promise<ScanAuthorizationInterface> {
    const authorization = await this.get(this.backUrl + "/scan-authorization/one/" + dateId);
    return authorization.data;
  }
  async createDateAuthorization(eventDateId: number): Promise<ScanAuthorizationInterface> {
    const data: NewAuthorizationRequest = {
      eventDate: eventDateId
    }
    const authorization = await this.post(this.backUrl + "/scan-authorization/add", data)
    return authorization.data
  }
  async deleteDateAuthorization(authorizationId: number) {
    return this.delete(this.backUrl + "/scan-authorization/" + authorizationId);
  }
  async currentUserAuthorizations(): Promise<ScanAuthorizationInterface[]> {
    const authorizations = await this.get(this.backUrl + "/scan-authorization/current-user");
    return authorizations.data
  }

  async getAuthorizationEventDate(token: string): Promise<EventDateInterface> {
    const authorization = await this.get(this.backUrl + "/scan-authorization/getbytoken/" + token);
    return authorization.data.eventDate
  }


}
