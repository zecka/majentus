import { FetchApi } from "./fetch-api";
import { CreateTicketRequestData, ScanTicketRequest, OrderResponse } from "@shared/interfaces";

export class TicketService extends FetchApi {
  async getAll() {
    const tickets = await this.get(this.backUrl + "/tickets")
    return tickets?.data;
  }

  async getCurrentSellerTickets() {
    const tickets = await this.get(this.backUrl + "/tickets/seller");
    return tickets.data;
  }

  async create(data: CreateTicketRequestData): Promise<OrderResponse> {
    const tickets = await this.post(this.backUrl + "/tickets/create", data);
    return tickets.data;
  }

  async scan(ticketId: number, ticketToken: string, authorization: string) {
    const data: ScanTicketRequest = {
      ticketId: String(ticketId),
      ticketToken,
      authorization
    }
    const ticket = await this.post(this.backUrl + "/tickets/scan", data);
    return ticket.data;
  }

}
