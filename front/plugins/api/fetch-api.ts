import { IFetchApi } from '.'
import { Context } from "@nuxt/types/app/index.d"

type AxiosMethods = 'get' | 'post' | 'put' | 'delete' | 'getBlob';

export class FetchApi implements IFetchApi {
  private context: Context
  public axios: any;
  public backUrl: string = process.env.NUXT_BACK_URL!;
  constructor(context: Context) {
    this.context = context;
    this.axios = this.context.app.$axios;
  }
  async get(url: string) {
    return await this.request("get", url)
  }
  async getBlob(url: string) {
    return await this.request("getBlob", url)
  }
  async post(url: string, data?: Object) {
    return await this.request("post", url, data)
  }
  async put(url: string, data?: Object) {
    return await this.request("put", url, data)
  }
  async delete(url: string, data?: Object) {
    return await this.request("delete", url, data)
  }

  async request(method: AxiosMethods, url: string, data?: Object, options?: Object) {
    let timer = setTimeout(() => {
      this.context.error({ "message": "TIMEOUT", statusCode: 504 })
    }, 15000)
    try {
      let response = null;
      switch (method) {
        case 'get':
          response = await this.context.app.$axios.get(url)
          break;
        case 'getBlob':
          response = await this.context.app.$axios.get(url, { responseType: 'blob' })
          break;
        case 'post':
          response = await this.context.app.$axios.post(url, data)
          break;
        case 'put':
          response = await this.context.app.$axios.put(url, data)
          break;
        case 'delete':
          response = await this.context.app.$axios.delete(url, data)
          break;
        default:
          throw new Error("Following method doesn't exist on axios: " + method)
      }
      clearTimeout(timer);
      return response;
    } catch (error) {
      if (!error.response) {
        this.context.error({ message: "NO_RESPONSE", statusCode: 503 })
      }
      clearTimeout(timer);
      throw error;
    }
  }
}

