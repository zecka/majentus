import { Plugin } from '@nuxt/types'
import { FetchApi } from './fetch-api';
import { EventService } from './event-service';
import { EventDateService } from './eventdate-service';
import { CategoryService } from './category-service';
import { TicketService } from './ticket-service';
import { UploadService } from './upload-service';
import { IApi } from '.';
import { UserService } from './user-service';
import { OrderService } from './order-service';
import { StatsService } from './stats-service';
import { ScanService } from './scan-service';
import { MjContactService } from './contact-service';

declare module 'vue/types/vue' {
  interface Vue {
    $api: IApi,
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $api: IApi,
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $api: IApi,
  }
}

const api: Plugin = (context, inject) => {
  const apiObject: IApi = {
    request: new FetchApi(context),
    event: new EventService(context),
    user: new UserService(context),
    eventDate: new EventDateService(context),
    category: new CategoryService(context),
    ticket: new TicketService(context),
    upload: new UploadService(context),
    order: new OrderService(context),
    stats: new StatsService(context),
    scan: new ScanService(context),
    contact: new MjContactService(context)
  }
  inject('api', apiObject)
}

export default api
