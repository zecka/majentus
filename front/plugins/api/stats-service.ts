import { FetchApi } from "./fetch-api";
import { EventDatePricingStats } from '@shared/interfaces';
export class StatsService extends FetchApi {

  private baseService: string = this.backUrl + "/statistiques"
  async eventDatesPricings(): Promise<EventDatePricingStats[]> {
    const stats = await this.get(this.baseService + "/dates-pricing");
    return stats.data;
  }


}
