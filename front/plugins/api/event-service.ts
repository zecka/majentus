import { Context } from "@nuxt/types/app/index.d"
import { FetchApi } from "./fetch-api";
import { EventItemInterface, NewEventInterface } from "~/../shared/interfaces/EventItemInterface";
import { EditEventInterface } from '../../../shared/interfaces/EventItemInterface';
import { NewPricingRequest, UpdatePricingRequest } from "@shared/interfaces/RequestDataInterface"

export class EventService extends FetchApi {
  async getAll() {
    const events = await this.get(this.backUrl + "/events")
    return events.data;
  }
  async currentUser() {
    const events = await this.get(this.backUrl + "/events/current-user")
    return events.data ? events.data : [];
  }
  async getUpcommingEvents() {
    const events = await this.get(this.backUrl + "/events/upcomings")
    return events.data;
  }
  async getPastEvents() {
    const events = await this.get(this.backUrl + "/events/past")
    return events.data;
  }
  async getOne(id: number | string): Promise<EventItemInterface> {
    const event = await this.get(this.backUrl + "/events/" + id)
    return event.data;
  }
  async remove(eventId: number): Promise<any> {
    return await this.delete(this.backUrl + "/events/" + eventId);
  }
  async update(event: EditEventInterface): Promise<any> {
    const newEvent = await this.put(this.backUrl + "/events", event)
    return newEvent.data;
  }
  async create(event: NewEventInterface): Promise<EventItemInterface> {
    const newEvent = await this.post(this.backUrl + "/events", event)
    return newEvent.data;
  }

  addPricing(pricing: NewPricingRequest) {
    return this.post(this.backUrl + "/pricings", pricing)
  }
  updatePricing(pricing: UpdatePricingRequest) {
    return this.put(this.backUrl + "/pricings", pricing)
  }
  deletePricing(pricingId: number) {
    return this.delete(this.backUrl + "/pricings/" + pricingId);
  }

}
