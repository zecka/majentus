import { Context } from "@nuxt/types/app/index.d"
import { FetchApi } from "./fetch-api";
import { EventDateInterface, INewEventDate } from "~/../shared/interfaces/EventDateInterface";
import { EventDateEditInterface } from '../../../shared/interfaces/EventDateInterface';

export class EventDateService extends FetchApi {
  async getAll() {
    const events = await this.get(this.backUrl + "/event-date")
    return events.data;
  }
  async getOne(id: number): Promise<EventDateInterface> {
    const event = await this.get(this.backUrl + "/event-date/date/" + id)
    return event.data;
  }
  async getUpcommingDates(): Promise<EventDateInterface[]> {
    const eventDates = await this.get(this.backUrl + "/event-date/upcomings")
    return eventDates.data;
  }
  async getPastDates(): Promise<EventDateInterface[]> {
    const eventDates = await this.get(this.backUrl + "/event-date/past")
    return eventDates.data;
  }
  async update(data: EventDateEditInterface): Promise<EventDateInterface> {
    const eventDates = await this.put(this.backUrl + "/event-date", data)
    return eventDates.data;
  }
  async create(data: INewEventDate): Promise<EventDateInterface> {
    const eventDate = await this.post(this.backUrl + "/event-date", data);
    return eventDate.data;
  }
  async remove(eventDateId: number): Promise<any> {
    return await this.delete(this.backUrl + "/event-date/date/" + eventDateId);
  }
}
