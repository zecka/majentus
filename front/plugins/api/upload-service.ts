import { FetchApi } from "./fetch-api";

export class UploadService extends FetchApi {
  async upload(file: File) {
    var formData = new FormData();
    formData.append("image", file);
    const result = await this.axios.post(this.backUrl + '/media/image', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    return result;
  }
  async remove(id: number) {
    const result = await this.delete(this.backUrl + '/media/' + id)
    return result;
  }

}
