
export const mjErrors: MjErrors = {
  EVENTDATE_HAVE_TICKETS: {
    message: "Des tickets on déjà été vendu pour cette date",
    additionnalInfo: "Vous ne pouvez plus la supprimer, pour la désactiver changer son status par annulé"
  },
  PRICING_HAVE_TICKET: {
    message: "Des tickets on déjà été vendu pour ce tarif",
    additionnalInfo: "Vous ne pouvez plus le supprimer"
  },
  UNKNOW_ERROR: {
    message: "Erreur inconnue",
    additionnalInfo: "Veuillez ré-essayer plus tard"
  },
  INVALID_PASSWORD: {
    message: "nom d'utilisateur ou mot de passe incorrect",
    timeout: 3000
  },
  NETWORK_ERROR: {
    message: "Erreur de connexion",
    additionnalInfo: "Erreur lors de la connexion au serveur, veuillez ré-essayer plus tard"
  }
}
