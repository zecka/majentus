type MjErrorCode = "EVENTDATE_HAVE_TICKETS" | 'UNKNOW_ERROR' | 'NETWORK_ERROR' | 'INVALID_PASSWORD' | 'PRICING_HAVE_TICKET';

interface IMj {
  color(color: string): string;
  getError(errorCode: MjErrorCode): MjError
  openSnackbar(content: string, color: SnackbarColor = 'success', timeout: number = 3000): void
  openErrorSnackbar(error: any, timeout: number = -1): void
  openErrorCodeSnackbar(errorCode: MjErrorCode, timeout: number = -1)
  async download(url: string): void
  asyncRouterReplace(route: any): Promise<any>
  handleError(e: any)
}

// enum MjErrorCode { EVENTDATE_HAVE_TICKETS, Expand, EndExpand, None }

interface MjError {
  message: string
  additionnalInfo?: string
  timeout?: number
}
type MjErrors = {
  [code in MjErrorCode]?: MjError
} & {
  'UNKNOW_ERROR': MjError
}


/*
interface MjErrors {
  [key: MjErrorCode]: MjError;
}
*/
