import { Plugin } from '@nuxt/types'
import { Context } from '@nuxt/types/app/index.d';
import { replaceAll } from '~/inc/helpers';
import { mjErrors } from './mjErrors';
import MjBus from './mjbus';
import { saveAs } from 'file-saver'

declare module 'vue/types/vue' {
  interface Vue {
    $mj: IMj
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $mj: IMj
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $mj: IMj
  }
}




const mj: Plugin = (context, inject) => {
  const mjObject: IMj = {
    color: (color: string) => {
      if (context.$vuetify.theme.dark) {
        color = replaceAll(color, 'lighten', '%%D%%')
        color = replaceAll(color, 'darken', '%%L%%')
        color = replaceAll(color, 'white', '%%B%%')
        color = replaceAll(color, 'black', '%%W%%')
        color = replaceAll(color, '%%D%%', 'darken')
        color = replaceAll(color, '%%L%%', 'lighten')
        color = replaceAll(color, '%%B%%', 'black')
        color = replaceAll(color, '%%W%%', 'white')
      }
      return color;
    },
    getError(errorCode: MjErrorCode): MjError {
      const error = mjErrors[errorCode] ? mjErrors![errorCode] : mjErrors.UNKNOW_ERROR;
      return (typeof error === 'undefined') ? mjErrors.UNKNOW_ERROR : error;
    },
    openSnackbar(content: string, color: SnackbarColor = 'success', timeout: number = 3000) {
      MjBus.$emit('open-snackbar', content, color, timeout)
    },
    openErrorSnackbar(e: any, timeout: number = -1) {
      let errorCode: MjErrorCode = (e.response) ? e.response.data.message : 'NETWORK_ERROR';
      this.openErrorCodeSnackbar(errorCode);
    },
    openErrorCodeSnackbar(errorCode: MjErrorCode, timeout: number = -1) {
      const error = this.getError(errorCode);
      let message = error.message;
      if (error.additionnalInfo) {
        message = `<strong>${error.message}</strong><br />${error.additionnalInfo}`
      }
      if (error.timeout) {
        timeout = error.timeout;
      }
      this.openSnackbar(message, 'error', timeout)
    },
    async download(url: string) {
      const response = await context.$api.request.getBlob(url)
      saveAs(new Blob([response.data]), 'ticket.pdf')
    },
    asyncRouterReplace(route: any) {
      return new Promise(resolve => {
        resolve(context.app.router!.replace(route))
      })
    },
    handleError(e: any) {
      if (e.response.data && e.response.data.message) {
        context.error({
          statusCode: e.response.data.httpCode,
          message: e.response.data.message
        })
      } else {
        context.error({
          statusCode: 500,
          message: 'Erreur inconnue'
        })
      }
    }
  }
  inject('mj', mjObject)
}

export default mj
