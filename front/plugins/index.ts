import { IApi } from "./api";

declare module 'vue/types/vue' {
  interface Vue {
    $api: IApi,
    $mj: IMj
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $api: IApi,
    $mj: IMj
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $api: IApi,
    $mj: IMj
  }
}
