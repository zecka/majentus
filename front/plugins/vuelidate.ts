import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VueRx from 'vue-rx'
// install vue-rx
Vue.use(VueRx)
Vue.use(Vuelidate)
