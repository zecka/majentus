
import Vue from 'vue'
import TextField from '@/components/core/field/TextField.vue'
import CustomField from '@/components/core/field/CustomField.vue'
import EmailField from '@/components/core/field/EmailField.vue'
import CategoriesField from '@/components/core/field/CategoriesField.vue'
import NumberField from '@/components/core/field/NumberField.vue'
import QuantityField from '@/components/core/field/QuantityField.vue'
import DateTimeField from '@/components/core/field/DateTimeField.vue'
import ImageField from '@/components/core/field/ImageField.vue'
import PasswordField from '@/components/core/field/PasswordField.vue'
import Typography from '@/components/core/Typography.vue';
import DashboardTitle from '@/components/dashboard/DashboardTitle.vue'
import DashboardSubtitle from '@/components/dashboard/DashboardSubtitle.vue'
import InlineSvg from "@/components/core/svg/InlineSvg.vue";
import AddressField from "@/components/core/field/AddressField.vue";
import AvatarField from "@/components/core/field/AvatarField.vue";
// and register it

Vue.component('CustomField', CustomField)
Vue.component('EmailField', EmailField)
Vue.component('TextField', TextField)
Vue.component('QuantityField', QuantityField)
Vue.component('NumberField', NumberField)
Vue.component('CategoriesField', CategoriesField)
Vue.component('DateTimeField', DateTimeField)
Vue.component('Typography', Typography)
Vue.component('PasswordField', PasswordField)
Vue.component('ImageField', ImageField)
Vue.component('DashboardTitle', DashboardTitle)
Vue.component('DashboardSubtitle', DashboardSubtitle)
Vue.component('InlineSvg', InlineSvg)
Vue.component('AddressField', AddressField)
Vue.component('AvatarField', AvatarField)
