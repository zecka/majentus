var os = require('os')

export const myLocalIp = () => {
  var interfaces = os.networkInterfaces()
  var addresses = []
  for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
      var address = interfaces[k][k2]
      if (address.family === 'IPv4' && !address.internal) {
        addresses.push(address.address)
      }
    }
  }
  return addresses[0]
}

export const defineLocalUrls = () => {}

export const getEnv = () => {
  defineLocalUrls()
  let baseUrl = process.env.NUXT_BASE_URL
  if (process.env.NUXT_ENV === 'local') {
    baseUrl = 'http://' + myLocalIp() + ':' + process.env.NUXT_PORT
  }
  return {
    NUXT_BACK_URL: process.env.NUXT_BACK_URL,
    NUXT_BASE_URL: baseUrl,
    NUXT_ENV: process.env.NUXT_ENV,
    NUXT_PORT: process.env.NUXT_PORT
  }
}

export default myLocalIp
