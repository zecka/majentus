const qrcode = require('qrcode-terminal')
export const welcomeQr = (env) => {
  qrcode.generate(env.NUXT_BASE_URL)
}
