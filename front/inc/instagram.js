
const cleanFeed = (data) => {
	const posts = data.graphql.user.edge_owner_to_timeline_media.edges
	const feed = []
	posts.forEach((post, index)=>{
		const feedItem = {}
		feedItem.text = post['node']['edge_media_to_caption']['edges'][0]['node']['text']
		feedItem.comments_count = post['node']['edge_media_to_comment']['count'];
		feedItem.likes_count = post['node']['edge_liked_by'];
		feedItem.link= 'https://www.instagram.com/p/'.post['node']['shortcode'].'/';
		feedItem.full = post['node']['display_url'];
		feedItem.xsmall = post['node']['thumbnail_resources'][0]['src'];
		feedItem.small = post['node']['thumbnail_resources'][1]['src'];
		feedItem.medium = post['node']['thumbnail_resources'][2]['src'];
		feedItem.large = post['node']['thumbnail_resources'][3]['src'];
		feedItem.xlarge = post['node']['thumbnail_resources'][4]['src'];
		feed.push(feedItem)
	})
	return feed;
}

const getInstagramFeed = async (username) => {
	try{
		const instaUrl = "https://www.instagram.com/"+username+"/?__a=1"
		const response = await fetch(url)
		const dataJson = await response.json()
		return cleanFeed(dataJson)
	}catch(e){
		console.error("ERROR WHILE FETCH INSTAGRAM")
	}
}

const exampleUsage = async () => {
	const feed = await getInstagramFeed("la_gamelle.ch")
}
