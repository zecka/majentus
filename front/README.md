# front-nuxt

> My solid Nuxt.js project

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Information important

Afin de bien comprendre le fonctionnement de NUXT il faut comprende sont lifecycle:

[https://fr.nuxtjs.org/guides/concepts/nuxt-lifecycle/](https://fr.nuxtjs.org/guides/concepts/nuxt-lifecycle/)
