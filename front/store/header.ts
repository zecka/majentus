import { Module, VuexModule, Mutation } from "vuex-module-decorators";


@Module({
  name: "header",
  stateFactory: true,
  namespaced: true
})
export default class HeaderStore extends VuexModule {

  headerIsOpen: boolean = false;
  get isOpen() {
    return this.headerIsOpen
  }
  @Mutation
  toggle() {
    this.headerIsOpen = !this.headerIsOpen
  }
  @Mutation
  close() {
    this.headerIsOpen = false
  }
  @Mutation
  open() {
    this.headerIsOpen = true
  }

}
