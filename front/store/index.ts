import { actionTree } from 'nuxt-typed-vuex'
export const state = () => ({
  things: [] as string[],
  name: 'Me',
})


export const actions = actionTree(
  { state },
  {
    async nuxtServerInit({ dispatch }: any, { app, query, route, error }: any) {

    },
    async nuxtClientInit(dontcare, context) {
    }
  }
)

export type RootState = ReturnType<typeof state>
