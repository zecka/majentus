import { Roles } from './Enum'
export interface UserItemInterface {
    id: number;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    fullName: string;
    role: Roles;
}
