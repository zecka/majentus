import { EventDateInterface } from ".";

export interface ScanAuthorizationInterface {
    id: number
    token: string
    createdAt?: Date,
    eventDate: EventDateInterface
}