import { EventDateInterface } from './EventDateInterface';
import { PricingItemInterface } from './PricingInterface';
import { AddressInterface } from './Address';

export type PricingRequestData = {
    id: number
    quantity: number
};
export type CustomerRequestData = {
    firstName: string
    lastName: string
    email: string
    address: AddressInterface
}
export type CreateTicketRequestData = CustomerRequestData & {
    eventDateId: number
    eventDate?: EventDateInterface
    pricing: PricingRequestData[]
};
export type BuyTicketRequestData = CreateTicketRequestData & {
    token: string
}

export type ScanResult = {
    type: string,
    message: string,
    payload: string | Object
}
export interface ScanTicketRequest {
    ticketToken: string
    authorization: string
    ticketId: string
}
export interface NewUserRequestData {
    username: string
    email: string
    firstName: string
    lastName: string
    password: string
}

export type UserAvailabilityRequest = {
    username?: string
    email?: string
}
export type UserAvailabilityResponse = {
    email: null | boolean
    username: null | boolean
}

export type NewPricingRequest = {
    eventId: number
    title: string
    price: number
}
export interface PricingFormItem {
    id?: number
    title?: string
    price?: number
}
export interface UpdatePricingRequest {
    id: number
    title: string
    price: number
}

export interface UpdateEventDateRequest {
    id: number
    date: string
    status: string
}

export interface NewEventDateRequest {
    date: string
    status: string
    event: number
}

export interface TicketItemInterface {
    id: number
    used: boolean
    firstName: string
    lastName: string
    email: string
    token: string
    createdAt: Date
    updatedAt: Date
    pricing: PricingItemInterface
}
export interface OrderResponse {
    id: number
    token: string
    email: string
    tickets: TicketItemInterface[]
}

export interface UpdateUserRequest {
    firstName?: string
    lastName?: string
    email?: string
    oldPassword?: string
    password?: string
    avatar?: any
    changeEmail?: {
        id: number,
        email: string,
        cancelToken: string
    }
}
export interface StripeEntityRequest {
    secretKey?: string
    publicKey?: string
    id?: number
}

export interface NewAuthorizationRequest {
    eventDate: number
}

export interface ContactRequest {
    name: string
    email: string
    subject: string
    message: string
    sendCopy: boolean
}
export interface ContactResponse {
    success: boolean
}