export type Roles = 'admin' | 'seller';
export enum EventStatus {
    Pending = "pending",
    Open = "open",
    Cancel = "cancel",
    Closed = "closed"
}