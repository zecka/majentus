import { UserItemInterface } from './UserItemInterface';
export interface MediaItemInterface {
    id: number;

    path: string;

    mimetype: string;

    user?: UserItemInterface;

    createdAt: Date;

    updatedAt: Date;

}