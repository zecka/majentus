import { EventItemInterface } from './EventItemInterface';
import { EventStatus } from './Enum';
import { UserItemInterface } from '@shared/interfaces';
export interface EventDateInterface {
    status: EventStatus;
    id: number
    date: Date
    sold: number
    event?: EventItemInterface,
    user?: UserItemInterface
}
export interface INewEventDate {
    event: number,
    date?: Date,
    status: EventStatus
}
export interface EventDateEditInterface {
    id?: number,
    date?: Date,
    status?: EventStatus,
}