export interface PlaceholderItem {
    userId: string;
    id: string;
    title: string;
    body: string;
}
