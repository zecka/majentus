import { UserItemInterface } from './UserItemInterface';
import { EventStatus } from './Enum';
import { EventDateInterface } from './EventDateInterface';
import { MediaItemInterface } from './MediaItemInterface';
export interface EventItemInterface {
    id: number;
    title: string;
    description?: string;
    start: Date;
    end: Date;
    status: EventStatus
    user: UserItemInterface;
    categories: Array<any>;
    pricings: Array<any>
    capacity: number
    duration: number,
    dates: EventDateInterface[]
    cover: MediaItemInterface,
    gateway: {
        gateway: string
        publicKey: string
    }
}

export interface NewEventInterface {
    id: undefined,
    title?: string
    categories?: number[]
    capacity?: number
    duration?: number
    cover?: number
}
export interface EditEventInterface {
    id?: number
    title?: string
    description?: string
    categories?: number[]
    capacity?: number
    duration?: number
    cover?: number
}