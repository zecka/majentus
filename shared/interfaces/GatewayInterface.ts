export interface GatewayInterface {
    id: number
    secretKey: string;
    publicKey: string;
    gateway: string;
}