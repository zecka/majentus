export type AddressInterface = {
    line1?: string,
    city?: string,
    state?: string,
    postal_code?: number,
    country?: "CH" | "FR",
}
