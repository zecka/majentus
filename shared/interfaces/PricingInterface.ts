import { EventItemInterface } from './EventItemInterface';
export interface PricingItemInterface {
    id: number;
    title: string;
    price: number;
    event?: EventItemInterface;
}